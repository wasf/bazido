Rails.application.routes.draw do

 # ======== Learn ===============
  get '/users/:user_id/joined' => 'learn/courses#joined', as: 'user_joined_courses'
  namespace :learn do
    get '/', to: redirect('/learn/courses/questions')
    resources :courses, param: :internal_code do
      match :search, :on => :collection, :via => [:post, :get]
      get :questions, :on => :collection

      get :join, :on => :member
      get :leave, :on => :member

      resources :questions do
        resources :answers
        match :search, :on => :collection, :via => [:post, :get]
      end

      resources :discussions do
        resources :points
        match :search, :on => :collection, :via => [:post, :get]
      end

    end
  end
 # ====================================================
  resources :messages, only: [:show, :index] do
    resources :responses, only: [:create]
  end
  # Make messages for different applications namespaces
    # Accommodations
    # get '/market/accommodations/:id/messages/new' => 'messages#new', as: 'new_market_accommodation_message'
  # ===================================================
  namespace :forum do
    get '/' => 'threads#index', as: 'index'
    resources :threads do
      resources :comments, only: [:create, :destroy, :edit, :update]
       get :watching, :on => :collection
    end
  end

  #get 'index/index'
  get '/about-us' => 'index#about', as: 'about'
  get '/terms-and-privacy' => 'index#terms', as: 'terms_and_privacy'
  get '/redirect' => 'index#redirect', as: 'redirect'
  resources :index, :only => [:index]

  devise_for :users, :controllers => {registrations: 'registrations' }
  devise_scope :user do
    resource :registration, only: [:new, :create, :edit, :update],
      path: 'users',
      path_names: {new: 'sign_up'},
      controller: 'devise/registrations',
      as: :user_registration do
        get :cancel
      end
  end
  # mount Market::Engine => 'market', :as => 'market_engine'
  
  resources :users, :except => [:new, :create, :destroy] do
    get 'messages/new' => 'messages#new'
    post 'messages' => 'messages#create', as: 'messages'
    get :market, :on => :member
    # get :payments, :on => :member
    # get :advertisements, :on => :member
    # get :sponsor, :on => :member

  end

  namespace :advertise do

    get '/pricing' => 'index#pricing', as: 'pricing'

    resources :advertisements do
      get :activate, on: :member
    end

    resources :sponsors do
      get :activate, on: :member
    end

    resources :payments, only: [:new, :create, :index]
  end
  
  namespace :market do
     get '/' => 'index#index'

     resources :refreshes, :only => [:update, :refresh] do
       get :refresh, :on => :member
     end

     resources :accommodations do
       
       get 'messages/new' => '/messages#new'
       post 'messages' => '/messages#create', as: 'messages'

       match :search, :on => :collection, :via => [:post, :get]
       # get :search, :on => :collection
       # post :search, :on => :collection
     end

     resources :books do

       get 'messages/new' => '/messages#new'
       post 'messages' => '/messages#create', as: 'messages'
       match :search, :on => :collection, :via => [:post, :get]
       # get :search, :on => :collection
       # post :search, :on => :collection
     end

     resources :contacts
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  # root 'index#index'
  root 'market/index#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
