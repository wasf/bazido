# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150217134535) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "academics", force: true do |t|
    t.integer  "profile_id"
    t.string   "institution", null: false
    t.string   "department",  null: false
    t.string   "code",        null: false
    t.string   "name",        null: false
    t.integer  "commence",    null: false
    t.integer  "graduation",  null: false
    t.text     "comment"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "academics", ["profile_id"], name: "index_academics_on_profile_id", using: :btree

  create_table "advertise_advertisements", force: true do |t|
    t.integer  "user_id"
    t.boolean  "banned",                      default: false
    t.string   "link"
    t.boolean  "paid",                        default: false
    t.datetime "last_link_update"
    t.datetime "last_image_update"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "alternative_text",            default: "Advertisement"
    t.string   "display_file_name"
    t.string   "display_content_type"
    t.integer  "display_file_size"
    t.datetime "display_updated_at"
    t.datetime "deleted_at"
    t.string   "display_mobile_file_name"
    t.string   "display_mobile_content_type"
    t.integer  "display_mobile_file_size"
    t.datetime "display_mobile_updated_at"
    t.string   "title",                       default: "No title given", null: false
  end

  add_index "advertise_advertisements", ["user_id"], name: "index_advertise_advertisements_on_user_id", using: :btree

  create_table "advertise_appearances", force: true do |t|
    t.string   "appearance_type"
    t.integer  "appearance_id"
    t.datetime "show_at"
    t.boolean  "locked"
    t.boolean  "free"
    t.decimal  "cost"
    t.string   "institution_website"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "advertise_appearances", ["appearance_type", "appearance_id", "show_at"], name: "index_on_appearance_show_at", using: :btree

  create_table "advertise_dateables", force: true do |t|
    t.string   "dateable_type"
    t.integer  "dateable_id"
    t.date     "display_date",                                                 null: false
    t.boolean  "locked",                                       default: false
    t.boolean  "free",                                         default: false
    t.decimal  "cost",                precision: 15, scale: 3
    t.string   "institution_website"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
  end

  add_index "advertise_dateables", ["dateable_id"], name: "index_advertise_dateables_on_dateable_id", using: :btree
  add_index "advertise_dateables", ["dateable_type", "dateable_id"], name: "index_advertise_dateables_on_dateable_type_and_dateable_id", using: :btree
  add_index "advertise_dateables", ["dateable_type"], name: "index_advertise_dateables_on_dateable_type", using: :btree
  add_index "advertise_dateables", ["institution_website", "cost"], name: "index_advertise_dateables_on_institution_website_and_cost", using: :btree
  add_index "advertise_dateables", ["institution_website"], name: "index_advertise_dateables_on_institution_website", using: :btree

  create_table "advertise_dates", force: true do |t|
    t.string   "dateable_type"
    t.integer  "dateable_id"
    t.datetime "show_at"
    t.boolean  "locked"
    t.boolean  "free"
    t.decimal  "cost"
    t.string   "institution_website"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "advertise_dates", ["dateable_type", "dateable_id", "show_at"], name: "index_on_dateable_date_show_on", using: :btree

  create_table "advertise_displays", force: true do |t|
    t.string   "displayable_type"
    t.integer  "displayable_id"
    t.datetime "show_at"
    t.boolean  "locked",                                       default: false
    t.boolean  "free",                                         default: false
    t.decimal  "cost",                precision: 12, scale: 3, default: 0.0
    t.string   "institution_website"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.integer  "consecutive_days",                             default: 1,     null: false
    t.datetime "show_til"
  end

  add_index "advertise_displays", ["displayable_type", "displayable_id", "show_at"], name: "index_on_advertise_display_show_at", using: :btree
  add_index "advertise_displays", ["institution_website", "show_at", "show_til"], name: "index_institution_show_at_til", using: :btree

  create_table "advertise_images", force: true do |t|
    t.string   "imageable_type"
    t.string   "imageable_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "display_file_name"
    t.string   "display_content_type"
    t.integer  "display_file_size"
    t.datetime "display_updated_at"
  end

  add_index "advertise_images", ["imageable_type", "imageable_id"], name: "index_advertise_images_on_imageable_type_and_imageable_id", using: :btree

  create_table "advertise_payments", force: true do |t|
    t.integer  "user_id"
    t.decimal  "amount",         precision: 15, scale: 3
    t.string   "transaction_id"
    t.datetime "date_paid"
    t.boolean  "refunded",                                default: false
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.text     "params"
    t.string   "invoice"
  end

  add_index "advertise_payments", ["user_id"], name: "index_advertise_payments_on_user_id", using: :btree

  create_table "advertise_sponsors", force: true do |t|
    t.integer  "user_id"
    t.boolean  "banned",               default: false
    t.string   "link"
    t.string   "image"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "display_file_name"
    t.string   "display_content_type"
    t.integer  "display_file_size"
    t.datetime "display_updated_at"
    t.datetime "deleted_at"
    t.string   "title",                default: "No title given"
    t.boolean  "paid",                 default: false
    t.string   "alternative_text",     default: "Sponsor"
  end

  add_index "advertise_sponsors", ["user_id"], name: "index_advertise_sponsors_on_user_id", using: :btree

  create_table "advertise_targets", force: true do |t|
    t.string   "target_type"
    t.integer  "target_id"
    t.datetime "show_at"
    t.boolean  "locked"
    t.boolean  "free"
    t.decimal  "cost"
    t.string   "institution_website"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "advertise_targets", ["target_type", "target_id", "show_at"], name: "index_on_target_show_at", using: :btree

  create_table "advertise_viewings", force: true do |t|
    t.string   "viewing_type"
    t.integer  "viewing_id"
    t.datetime "show_at"
    t.boolean  "locked"
    t.boolean  "free"
    t.decimal  "cost"
    t.string   "institution_website"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "advertise_viewings", ["viewing_type", "viewing_id", "show_at"], name: "index_on_viewing_show_at", using: :btree

  create_table "employments", force: true do |t|
    t.integer  "profile_id"
    t.string   "company",                          null: false
    t.string   "position",                         null: false
    t.string   "location",                         null: false
    t.boolean  "current_employer", default: false
    t.datetime "start_date"
    t.datetime "end_date"
    t.text     "description"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "employments", ["profile_id"], name: "index_employments_on_profile_id", using: :btree

  create_table "forum_comments", force: true do |t|
    t.integer  "user_id"
    t.integer  "thread_id"
    t.text     "content"
    t.string   "status"
    t.string   "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "forum_comments", ["thread_id", "user_id", "status"], name: "index_forum_comments_on_thread_id_and_user_id_and_status", using: :btree
  add_index "forum_comments", ["thread_id"], name: "index_forum_comments_on_thread_id", using: :btree
  add_index "forum_comments", ["user_id"], name: "index_forum_comments_on_user_id", using: :btree

  create_table "forum_threads", force: true do |t|
    t.integer  "user_id"
    t.string   "institution_website"
    t.string   "title"
    t.text     "content"
    t.string   "status"
    t.string   "reason"
    t.string   "category"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "forum_threads", ["institution_website", "category", "status"], name: "index_on_institution_category_status", using: :btree
  add_index "forum_threads", ["user_id", "status"], name: "index_forum_threads_on_user_id_and_status", using: :btree
  add_index "forum_threads", ["user_id"], name: "index_forum_threads_on_user_id", using: :btree

  create_table "learn_answers", force: true do |t|
    t.integer  "question_id"
    t.integer  "user_id"
    t.text     "content"
    t.string   "status"
    t.text     "sources"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "reason"
  end

  add_index "learn_answers", ["question_id", "user_id"], name: "index_learn_answers_on_question_id_and_user_id", using: :btree
  add_index "learn_answers", ["question_id"], name: "index_learn_answers_on_question_id", using: :btree
  add_index "learn_answers", ["user_id"], name: "index_learn_answers_on_user_id", using: :btree

  create_table "learn_attendees", force: true do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "learn_attendees", ["course_id", "user_id"], name: "index_learn_attendees_on_course_id_and_user_id", using: :btree
  add_index "learn_attendees", ["user_id"], name: "index_learn_attendees_on_user_id", using: :btree

  create_table "learn_courses", force: true do |t|
    t.string   "institution_website"
    t.integer  "user_id"
    t.string   "code"
    t.string   "name",                default: "Not given"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "internal_code"
  end

  add_index "learn_courses", ["institution_website", "user_id", "internal_code"], name: "index_on_institution-user-internal-code", using: :btree
  add_index "learn_courses", ["institution_website"], name: "index_learn_courses_on_institution_website", using: :btree

  create_table "learn_discussions", force: true do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.string   "title"
    t.text     "content"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "learn_discussions", ["course_id", "user_id"], name: "index_learn_discussions_on_course_id_and_user_id", using: :btree
  add_index "learn_discussions", ["course_id"], name: "index_learn_discussions_on_course_id", using: :btree
  add_index "learn_discussions", ["user_id"], name: "index_learn_discussions_on_user_id", using: :btree

  create_table "learn_points", force: true do |t|
    t.integer  "discussion_id"
    t.integer  "user_id"
    t.text     "content"
    t.string   "status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "learn_points", ["discussion_id", "user_id"], name: "index_learn_points_on_discussion_id_and_user_id", using: :btree
  add_index "learn_points", ["discussion_id"], name: "index_learn_points_on_discussion_id", using: :btree
  add_index "learn_points", ["user_id"], name: "index_learn_points_on_user_id", using: :btree

  create_table "learn_questions", force: true do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.string   "title"
    t.text     "content"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "category"
    t.string   "reason"
  end

  add_index "learn_questions", ["course_id", "category", "user_id"], name: "index_learn_questions_on_course_id_and_category_and_user_id", using: :btree
  add_index "learn_questions", ["course_id", "user_id"], name: "index_learn_questions_on_course_id_and_user_id", using: :btree
  add_index "learn_questions", ["course_id"], name: "index_learn_questions_on_course_id", using: :btree
  add_index "learn_questions", ["user_id"], name: "index_learn_questions_on_user_id", using: :btree

  create_table "market_accommodations", force: true do |t|
    t.string   "title",                                                     null: false
    t.text     "description"
    t.decimal  "deposit",            precision: 15, scale: 3, default: 0.0
    t.decimal  "rent",               precision: 15, scale: 3,               null: false
    t.string   "location"
    t.decimal  "first_payment",      precision: 15, scale: 3,               null: false
    t.datetime "occupation_date"
    t.string   "accommodation_type"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
  end

  create_table "market_books", force: true do |t|
    t.string   "title"
    t.string   "author"
    t.integer  "edition"
    t.string   "isbn"
    t.string   "condition"
    t.string   "age"
    t.decimal  "price",      precision: 15, scale: 3
    t.text     "comments"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "courses"
  end

  add_index "market_books", ["isbn"], name: "index_market_books_on_isbn", using: :btree
  add_index "market_books", ["price"], name: "index_market_books_on_price", using: :btree

  create_table "market_contacts", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "content"
    t.string   "private_name"
    t.boolean  "is_public"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "market_contacts", ["user_id"], name: "index_market_contacts_on_user_id", using: :btree

  create_table "market_contacts_refreshes", id: false, force: true do |t|
    t.integer "contact_id"
    t.integer "refresh_id"
  end

  add_index "market_contacts_refreshes", ["contact_id", "refresh_id"], name: "index_market_contacts_refreshes_on_contact_id_and_refresh_id", using: :btree

  create_table "market_images", force: true do |t|
    t.integer  "refresh_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "display_file_name"
    t.string   "display_content_type"
    t.integer  "display_file_size"
    t.datetime "display_updated_at"
  end

  add_index "market_images", ["refresh_id"], name: "index_market_images_on_refresh_id", using: :btree

  create_table "market_images_refreshes", id: false, force: true do |t|
    t.integer "image_id"
    t.integer "refresh_id"
  end

  add_index "market_images_refreshes", ["refresh_id", "image_id"], name: "index_market_images_refreshes_on_refresh_id_and_image_id", using: :btree

  create_table "market_necessities", force: true do |t|
    t.boolean  "furnished"
    t.boolean  "shared"
    t.boolean  "sharing"
    t.decimal  "rooms"
    t.boolean  "electricity"
    t.boolean  "water"
    t.boolean  "parking"
    t.boolean  "security"
    t.boolean  "laundry"
    t.decimal  "floor_area"
    t.integer  "accommodation_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "market_necessities", ["accommodation_id"], name: "index_market_necessities_on_accommodation_id", using: :btree

  create_table "market_refreshes", force: true do |t|
    t.string   "refreshable_type"
    t.integer  "refreshable_id"
    t.datetime "next_allowed"
    t.datetime "last_allowed"
    t.datetime "expires"
    t.integer  "status",              default: 0,     null: false
    t.boolean  "is_public",           default: false
    t.integer  "user_id"
    t.string   "institution_website",                 null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "market_refreshes", ["institution_website", "is_public", "status", "expires"], name: "index_on_institutionwebsite_is_public_status_expires", using: :btree
  add_index "market_refreshes", ["refreshable_type", "refreshable_id"], name: "index_market_refreshes_on_refreshable_type_and_refreshable_id", unique: true, using: :btree
  add_index "market_refreshes", ["user_id", "next_allowed"], name: "index_market_refreshes_on_user_id_and_next_allowed", using: :btree

  create_table "messages", force: true do |t|
    t.string   "messageable_type"
    t.integer  "messageable_id"
    t.integer  "user_id"
    t.string   "title"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "messages", ["messageable_type", "messageable_id", "user_id"], name: "index_on_messageable_user", using: :btree

  create_table "profiles", force: true do |t|
    t.integer  "user_id"
    t.string   "full_name",                                     null: false
    t.string   "username"
    t.string   "gender"
    t.text     "bio"
    t.string   "website"
    t.string   "original_institution_website",                  null: false
    t.string   "institution_website",                           null: false
    t.string   "role",                         default: "user", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "location"
    t.boolean  "has_payment",                  default: false
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "replies", force: true do |t|
    t.string   "replyable_type"
    t.integer  "replyable_id"
    t.text     "content"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "replies", ["replyable_type", "replyable_id"], name: "index_replies_on_replyable_type_and_replyable_id", using: :btree

  create_table "responses", force: true do |t|
    t.integer  "message_id"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "responses", ["message_id"], name: "index_responses_on_message_id", using: :btree

  create_table "transactions", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "card_number"
    t.integer  "month"
    t.integer  "year"
    t.integer  "verification_value"
    t.string   "card_type"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.decimal  "amount"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "watches", force: true do |t|
    t.string   "watchable_type"
    t.integer  "watchable_id"
    t.integer  "user_id"
    t.datetime "checked_at"
    t.string   "status"
    t.string   "application_space"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.boolean  "read"
  end

  add_index "watches", ["watchable_type", "watchable_id", "user_id", "status", "checked_at"], name: "index_on_user_watchable", using: :btree

  add_foreign_key "advertise_advertisements", "users"
  add_foreign_key "advertise_payments", "users"
  add_foreign_key "advertise_sponsors", "users"
end
