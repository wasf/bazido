class AddTitleAndPaidToAdvertiseSponsor < ActiveRecord::Migration
  def change
    add_column :advertise_sponsors, :title, :string, default: 'No title given'
    add_column :advertise_sponsors, :paid, :boolean, default: false
  end
end
