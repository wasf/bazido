class AddAlternativeTextToAdvertiseAdvertisement < ActiveRecord::Migration
  def change
    add_column :advertise_advertisements, :alternative_text, :string, default: 'Advertisement'
  end
end
