class CreateAdvertiseDisplays < ActiveRecord::Migration
  def change
    create_table :advertise_displays do |t|
      t.string :displayable_type
      t.integer :displayable_id
      t.datetime :show_at
      t.boolean :locked, default: false
      t.boolean :free, default: false
      t.decimal :cost, precision: 12, scale: 3, default: 0.0
      t.string :institution_website

      t.timestamps null: false
    end
    add_index :advertise_displays, [:displayable_type, :displayable_id, :show_at], name: 'index_on_advertise_display_show_at'
  end
end
