class AddCoursesToMarketBooks < ActiveRecord::Migration
  def change
    add_column :market_books, :courses, :string
  end
end
