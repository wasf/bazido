class AddReadToWatches < ActiveRecord::Migration
  def change
    add_column :watches, :read, :boolean, default: false
  end
end
