class AddReasonToLearnAnswer < ActiveRecord::Migration
  def change
    add_column :learn_answers, :reason, :string
  end
end
