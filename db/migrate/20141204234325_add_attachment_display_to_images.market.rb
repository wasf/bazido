# This migration comes from market (originally 20141203014527)
class AddAttachmentDisplayToImages < ActiveRecord::Migration
  def self.up
    change_table :market_images do |t|
      t.attachment :display
    end
  end

  def self.down
    remove_attachment :market_images, :display
  end
end
