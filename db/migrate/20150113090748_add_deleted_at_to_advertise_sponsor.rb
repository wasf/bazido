class AddDeletedAtToAdvertiseSponsor < ActiveRecord::Migration
  def change
    add_column :advertise_sponsors, :deleted_at, :datetime, default: nil
  end
end
