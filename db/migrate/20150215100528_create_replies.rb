class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.string :replyable_type
      t.integer :replyable_id
      t.text :content

      t.timestamps null: false
    end
    add_index :replies, [:replyable_type, :replyable_id]
  end
end
