class AddInternalCodeToLearnCourses < ActiveRecord::Migration
  def change
    add_column :learn_courses, :internal_code, :string, index: true
    add_index :learn_courses, [:institution_website, :user_id, :internal_code], name: 'index_on_institution-user-internal-code'
  end
end
