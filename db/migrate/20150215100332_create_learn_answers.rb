class CreateLearnAnswers < ActiveRecord::Migration
  def change
    create_table :learn_answers do |t|
      t.integer :question_id, index: true
      t.integer :user_id, index: true
      t.text :content
      t.string :status
      t.text :sources

      t.timestamps null: false
    end
    add_index :learn_answers, [:question_id, :user_id]
  end
end
