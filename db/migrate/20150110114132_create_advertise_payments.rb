class CreateAdvertisePayments < ActiveRecord::Migration
  def change
    create_table :advertise_payments do |t|
      t.references :user, index: true
      t.decimal :amount, precision: 15, scale: 3
      t.string :receipt
      t.datetime :date_paid
      t.boolean :refunded, default: false

      t.timestamps null: false
    end
    add_foreign_key :advertise_payments, :users
  end
end
