class AddConsecutiveDaysToAdvertiseDisplay < ActiveRecord::Migration
  def change
    add_column :advertise_displays, :consecutive_days, :integer, default: 1, unsigned: true, null: false
  end
end
