class CreateWatches < ActiveRecord::Migration
  def change
    create_table :watches do |t|
      t.string :watchable_type
      t.integer :watchable_id
      t.integer :user_id
      t.datetime :checked_at
      t.string :status
      t.string :application_space

      t.timestamps null: false
    end
    add_index :watches, [:watchable_type, :watchable_id, :user_id, :status, :checked_at], name: 'index_on_user_watchable'
  end
end
