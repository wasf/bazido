class AddAttachmentDisplayToAdvertiseSponsors < ActiveRecord::Migration
  def self.up
    change_table :advertise_sponsors do |t|
      t.attachment :display
    end
  end

  def self.down
    remove_attachment :advertise_sponsors, :display
  end
end
