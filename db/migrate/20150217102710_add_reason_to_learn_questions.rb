class AddReasonToLearnQuestions < ActiveRecord::Migration
  def change
    add_column :learn_questions, :reason, :string
  end
end
