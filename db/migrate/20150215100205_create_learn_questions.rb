class CreateLearnQuestions < ActiveRecord::Migration
  def change
    create_table :learn_questions do |t|
      t.integer :user_id, index: true
      t.integer :course_id, index: true
      t.string :title
      t.text :content
      t.string :status

      t.timestamps null: false
    end
    # add_index :learn_questions, [:course_id, :user_id]
  end
end
