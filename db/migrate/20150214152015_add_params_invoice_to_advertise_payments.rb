class AddParamsInvoiceToAdvertisePayments < ActiveRecord::Migration
  def change
    add_column :advertise_payments, :params, :text
    add_column :advertise_payments, :invoice, :string
  end
end
