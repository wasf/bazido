# This migration comes from market (originally 20141130021131)
class CreateMarketContacts < ActiveRecord::Migration
  def change
    create_table :market_contacts do |t|
      t.integer :user_id, index: true
      t.string :name
      t.string :content
      t.string :private_name
      t.boolean :is_public

      t.timestamps null: false
    end
    #add_foreign_key :market_contacts, :users
  end
end
