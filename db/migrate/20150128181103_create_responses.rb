class CreateResponses < ActiveRecord::Migration
  def change
    create_table :responses do |t|
      t.integer :message_id
      t.integer :user_id
      t.text :content

      t.timestamps null: false
    end
    add_index :responses, [:message_id]
  end
end
