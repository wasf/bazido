# This migration comes from market (originally 20141203014107)
class CreateMarketImages < ActiveRecord::Migration
  def change
    create_table :market_images do |t|
      # t.integer :user_id
      t.integer :refresh_id,index: true

      t.timestamps null: false
    end

    # add_index :market_images, [:user_id, :created_at]
  end
end
