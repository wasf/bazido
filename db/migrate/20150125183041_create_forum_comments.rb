class CreateForumComments < ActiveRecord::Migration
  def change
    create_table :forum_comments do |t|
      t.integer :user_id, index: true
      t.integer :thread_id, index: true
      t.text :content
      t.string :status
      t.string :reason

      t.timestamps null: false
    end
    add_index :forum_comments, [:thread_id, :user_id, :status]
  end
end
