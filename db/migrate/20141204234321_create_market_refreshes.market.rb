# This migration comes from market (originally 20141130020911)
class CreateMarketRefreshes < ActiveRecord::Migration
  def change
    create_table :market_refreshes do |t|
      t.string :refreshable_type
      t.integer :refreshable_id
      t.datetime :next_allowed
      t.datetime :last_allowed
      t.datetime :expires
      t.integer :status, null: false, default: 0
      t.boolean :is_public, default: false
      t.integer :user_id
      t.string :institution_website, null: false

      t.timestamps null: false
    end
    #add_foreign_key :market_refreshables, :users
    add_index :market_refreshes, [:refreshable_type, :refreshable_id], :unique => true
    add_index :market_refreshes, [:institution_website, :is_public, :status, :expires], name: 'index_on_institutionwebsite_is_public_status_expires'
    add_index :market_refreshes, [:user_id, :next_allowed]
  end
end
