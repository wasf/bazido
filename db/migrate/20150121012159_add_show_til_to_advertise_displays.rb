class AddShowTilToAdvertiseDisplays < ActiveRecord::Migration
  def change
    add_column :advertise_displays, :show_til, :datetime
    add_index :advertise_displays, [:institution_website, :show_at, :show_til], name: 'index_institution_show_at_til'
  end
end
