class AddHasPaymentToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :has_payment, :boolean, default: false
  end
end
