# This migration comes from market (originally 20141204140440)
class CreateMarketNecessities < ActiveRecord::Migration
  def change
    create_table :market_necessities do |t|
      t.boolean :furnished
      t.boolean :shared
      t.boolean :sharing
      t.decimal :rooms
      t.boolean :electricity
      t.boolean :water
      t.boolean :parking
      t.boolean :security
      t.boolean :laundry
      t.decimal :floor_area
      t.references :accommodation, index: true

      t.timestamps null: false
    end
  end
end
