class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :messageable_type
      t.integer :messageable_id
      t.integer :user_id
      t.string :title
      t.timestamps null: false
    end
    add_index :messages, [:messageable_type, :messageable_id, :user_id], name: 'index_on_messageable_user'
  end
end
