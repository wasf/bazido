# This migration comes from market (originally 20141130015532)
class CreateMarketAccommodations < ActiveRecord::Migration
  def change
    create_table :market_accommodations do |t|
      t.string :title, null: false
      t.text :description
      t.decimal :deposit, default: 0.0, :precision => 15, :scale => 3
      t.decimal :rent, null: false, :precision => 15, :scale => 3
      t.string :location
#      t.references :essential, index: true
#      t.references :refreshable, index: true
      t.decimal :first_payment, null: false, :precision => 15, :scale => 3
      t.datetime :occupation_date
      t.string :accommodation_type

      t.timestamps null: false
    end
 #   add_foreign_key :market_accommodations, :essentials
#    add_foreign_key :market_accommodations, :refreshables
  end
end
