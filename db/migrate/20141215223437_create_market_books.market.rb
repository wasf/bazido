# This migration comes from market (originally 20141215220557)
class CreateMarketBooks < ActiveRecord::Migration
  def change
    create_table :market_books do |t|
      t.string :title
      t.string :author
      t.integer :edition
      t.string :isbn
      t.string :condition
      t.string :age
      t.decimal :price, :precision => 15, :scale => 3 
      t.text :comments

      t.timestamps null: false
    end
    
    add_index :market_books, :isbn
    add_index :market_books, :price
  end
end
