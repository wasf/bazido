class AddAttachmentDisplayMobileToAdvertiseAdvertisements < ActiveRecord::Migration
  def self.up
    change_table :advertise_advertisements do |t|
      t.attachment :display_mobile
    end
  end

  def self.down
    remove_attachment :advertise_advertisements, :display_mobile
  end
end
