class CreateLearnAttendees < ActiveRecord::Migration
  def change
    create_table :learn_attendees do |t|
      t.integer :user_id, index: true
      t.integer :course_id

      t.timestamps null: false
    end
    add_index :learn_attendees, [:course_id, :user_id]
  end
end
