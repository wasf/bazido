class CreateLearnCourses < ActiveRecord::Migration
  def change
    create_table :learn_courses do |t|
      t.string :institution_website, index: true
      t.integer :user_id
      t.string :code
      t.string :name, default: 'Not given'

      t.timestamps null: false
    end
  end
end
