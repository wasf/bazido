class CreateAdvertiseAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertise_advertisements do |t|
      t.references :user, index: true
      t.boolean :banned, default: false
      t.string :link
      t.boolean :activated, default: false
      t.datetime :last_link_update
      t.datetime :last_image_update

      t.timestamps null: false
    end
    add_foreign_key :advertise_advertisements, :users
  end
end
