class CreateForumThreads < ActiveRecord::Migration
  def change
    create_table :forum_threads do |t|
      t.integer :user_id, index: true
      t.string :institution_website
      t.string :title
      t.text :content
      t.string :status
      t.string :reason
      t.string :category

      t.timestamps null: false
    end
    add_index :forum_threads, [:user_id, :status]
    add_index :forum_threads, [:institution_website, :category, :status], name: 'index_on_institution_category_status'
  end
end
