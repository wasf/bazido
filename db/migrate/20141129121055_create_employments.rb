class CreateEmployments < ActiveRecord::Migration
  def change
    create_table :employments do |t|
      t.references :profile, index: true
      t.string :company, null: false
      t.string :position, null: false
      t.string :location, null: false
      t.boolean :current_employer, default: false
      t.datetime :start_date
      t.datetime :end_date
      t.text :description

      t.timestamps null: false
    end
  end
end
