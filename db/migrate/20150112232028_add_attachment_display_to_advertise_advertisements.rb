class AddAttachmentDisplayToAdvertiseAdvertisements < ActiveRecord::Migration
  def self.up
    change_table :advertise_advertisements do |t|
      t.attachment :display
    end
  end

  def self.down
    remove_attachment :advertise_advertisements, :display
  end
end
