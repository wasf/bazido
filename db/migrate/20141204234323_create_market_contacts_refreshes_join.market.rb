# This migration comes from market (originally 20141130093300)
class CreateMarketContactsRefreshesJoin < ActiveRecord::Migration
  def change
    create_table :market_contacts_refreshes, id: false do |t|
      t.column :contact_id, :integer
      t.column :refresh_id, :integer
    end
    
    add_index :market_contacts_refreshes, [:contact_id, :refresh_id]
  end
end
