# This migration comes from market (originally 20141203015631)
class CreateMarketImagesRefreshesJoin < ActiveRecord::Migration
  def change
    create_table :market_images_refreshes, :id => false do |t|
      t.integer :image_id
      t.integer :refresh_id
    end
    add_index :market_images_refreshes, [:refresh_id, :image_id]
  end
end
