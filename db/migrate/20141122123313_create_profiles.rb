class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user, index: true
      t.string :full_name, null: false
      t.string :username, unique: true
      t.string :gender
      t.text :bio
      t.string :website
      t.string :original_institution_website, null: false
      t.string :institution_website, null: false
      t.string :role, null: false, :default => :user

      t.timestamps #null: false
    end
  end
end
