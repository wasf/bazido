class AddDeletedAtToAdvertiseAdvertisement < ActiveRecord::Migration
  def change
    add_column :advertise_advertisements, :deleted_at, :datetime, default: nil
  end
end
