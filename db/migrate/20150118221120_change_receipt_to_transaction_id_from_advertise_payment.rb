class ChangeReceiptToTransactionIdFromAdvertisePayment < ActiveRecord::Migration
  def change
    rename_column :advertise_payments, :receipt, :transaction_id
  end
end
