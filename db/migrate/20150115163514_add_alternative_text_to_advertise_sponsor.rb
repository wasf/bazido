class AddAlternativeTextToAdvertiseSponsor < ActiveRecord::Migration
  def change
    add_column :advertise_sponsors, :alternative_text, :string, default: 'Sponsor'
  end
end
