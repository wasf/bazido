class RenameNumberToCardNumberFromTransaction < ActiveRecord::Migration
  def change
    rename_column :transactions, :number, :card_number
  end
end
