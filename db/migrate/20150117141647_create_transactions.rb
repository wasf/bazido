class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :first_name
      t.string :last_name
      t.string :number
      t.integer :month
      t.integer :year
      t.integer :verification_value
      t.string :card_type

      t.timestamps null: false
    end
  end
end
