class CreateAcademics < ActiveRecord::Migration
  def change
    create_table :academics do |t|
      t.references :profile, index: true
      t.string :institution, null: false
      t.string :department, null: false 
      t.string :code, null: false 
      t.string :name, null: false 
      t.integer :commence, null: false 
      t.integer :graduation, null: false 
      t.text :comment

      t.timestamps null: false
    end
  end
end
