class ChangeActivatedToPaidFromAdvertiseAdvertisers < ActiveRecord::Migration
  def change
    rename_column :advertise_advertisements, :activated, :paid
  end
end
