class CreateAdvertiseSponsors < ActiveRecord::Migration
  def change
    create_table :advertise_sponsors do |t|
      t.references :user, index: true
      t.boolean :banned, default: false
      t.string :link
      t.string :image

      t.timestamps null: false
    end
    add_foreign_key :advertise_sponsors, :users
  end
end
