class AddTitleToAdvertiseAdvertisement < ActiveRecord::Migration
  def change
    add_column :advertise_advertisements, :title, :string, null: false, default: 'No title given'
  end
end
