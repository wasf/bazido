class AddCategoryToLearnQuestion < ActiveRecord::Migration
  def change
    add_column :learn_questions, :category, :string, index: true
    add_index :learn_questions, [:course_id, :category, :user_id]
  end
  
end
