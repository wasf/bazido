require 'rails_helper'

RSpec.describe "Users", :type => :feature do

  context "when not logged in" do

    describe "creating account" do

      it "should create account with valid information" do

        user = build(:user)
        visit new_user_registration_path

        fill_in 'user[profile_attributes][original_institution_website]', :with => user.profile.original_institution_website
        fill_in 'user[profile_attributes][full_name]', :with => user.profile.full_name
        fill_in 'user[email]', :with => user.email
        fill_in 'user[password]', :with => 'password'
        check('user[terms_and_conditions]')
        click_button 'user_submit'

        expect(page).to have_content("Your account has been created. Activate it by following the link we sent to your inbox to start posting.")

      end

      it "should fail with invalid information" do

        # user = build(:user)
        visit new_user_registration_path

        click_button 'user_submit'

        expect(page).to have_content("Email can't be blank")
        expect(page).to have_content("Password can't be blank")
        expect(page).to have_content("Terms and conditions must be accepted")
        expect(page).to have_content("Institution website is invalid")
        expect(page).to have_content("Full name can't be blank")
        expect(page).to have_content("Full name is too short")

      end

      # it "should fail when the email is taken" do

      # end

    end

    # describe "viewing a profile" do

    #   it "should show user infomation" do
    #     user = create(:user)

    #     visit user_path(user)

    #     expect(page).to have_content("Full1 Name 1")
    #   end
    # end

  end

  context "when logged in" do

  end

end
