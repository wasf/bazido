require 'rails_helper'

RSpec.describe Advertise::IndexController, :type => :controller do

  describe "GET pricing" do
    it "returns http success" do
      get :pricing
      expect(response).to have_http_status(:success)
    end
  end

end
