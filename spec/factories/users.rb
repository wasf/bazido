FactoryGirl.define do


  sequence :email do |n|
    "user-#{n}@test.dev"
  end

  factory :user do
    
    # profile

    email
    password 'password'
    association :profile, factory: [:profile, :user_role, :college_1]

    trait :admin do
      association :profile, factory: [:profile, :admin_role, :college_1]
    end

    trait :user_college_2 do
      association :profile, factory: [:profile, :user_role, :college_2]
    end

    trait :admin_college_2 do
      association :profile, factory: [:profile, :admin_role, :college_2]
    end

  end

end
