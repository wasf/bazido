FactoryGirl.define do

  sequence :full_name do |n|
    "Full#{n} Name#{n}"
  end

  factory :profile do
    full_name
    original_institution_website "college.edu"
    role "user"
    
    # College
    trait :college_1 do
      original_institution_website "1-college.edu"
    end

    trait :college_2 do
      original_institution_website "2-college.edu"
    end

    # Role
    trait :admin_role do
      role "admin"
    end
    
    trait :user_role do
      role "user"
    end

  end

end
