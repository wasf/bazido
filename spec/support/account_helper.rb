def user_login(email, password = 'password', remember_me = false)

  visit new_user_session_path

  fill_in 'user[email]', :with => email
  fill_in 'user[password]', :with => password

  if remember_me
    check 'user[remember_me]'
  end

  click_button 'commit'

end
