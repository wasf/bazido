module Market
  class RefreshSweeper < ActionController::Caching::Sweeper
    observe Refresh

    def after_save(o)
      expire_cache(o)
    end

    def after_destroy(o)
      expire_cache(o)
    end

    def expire_cache(o)
      Rails.cache.delete("/market/refreshes/#{o.id}")
      Rails.cache.delete("/market/refreshes/#{o.id}/refreshable/false")
      Rails.cache.delete("/market/refreshes/#{o.id}/refreshable/true")
    end
  end
end
