module Market
  class BookSweeper < ActionController::Caching::Sweeper
    observe Book

    def after_save(o)
      expire_cache(o)
    end

    def after_destroy(o)
      expire_cache(o)
    end

    def expire_cache(o)
      # raise o
      Rails.cache.delete("/market/books/#{o.id}")
    end
  end
end
