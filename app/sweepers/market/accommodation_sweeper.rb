module Market
  class AccommodationSweeper < ActionController::Caching::Sweeper
    observe Accommodation

    def after_save(o)
      expire_cache(o)
    end

    def after_destroy(o)
      expire_cache(o)
    end

    def expire_cache(o)
      # raise o
      Rails.cache.delete("/market/accommodations/#{o.id}")
    end
  end
end
