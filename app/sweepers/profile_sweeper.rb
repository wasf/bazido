
class ProfileSweeper < ActionController::Caching::Sweeper
  observe ::Profile

  def after_save(o)
    expire_cache(o)
  end

  def after_destroy(o)
    expire_cache(o)
  end

  def expire_cache(o)
    Rails.cache.delete("/users/#{o.user.id}")
  end
end
