
class AcademicSweeper < ActionController::Caching::Sweeper
  observe ::Academic

  def after_save(o)
    expire_cache(o)
  end

  def after_destroy(o)
    expire_cache(o)
  end

  def expire_cache(o)
    Rails.cache.delete("/users/#{o.profile.user.id}")
  end
end
