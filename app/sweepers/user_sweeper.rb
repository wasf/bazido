class UserSweeper < ActionController::Caching::Sweeper
  observe ::User

  def after_save(o)
    expire_cache(o)
  end

  def after_destroy(o)
    expire_cache(o)
  end

  def expire_cache(o)
    Rails.cache.delete("/users/#{o.id}")
  end
end
