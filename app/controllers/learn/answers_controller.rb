require 'learn/application_controller'

module Learn
  class AnswersController < ApplicationController

    before_action :load_question
    before_action :load_answer, except: [:create]
    load_and_authorize_resource through: :question

    respond_to :html

    def new
      @answer ||= @question.answers.build( user: curent_user)
    end

    def create
      @answer = @question.answers.build(answer_params)
      @answer.user = current_user

      if @answer.save
        flash[:notice] = 'Your post was successfully posted'
        redirect_to learn_course_question_path(@question.course, @question)
      else
        render action: :new
      end

    end

    def edit
    end

    def update
      @answer.assign_attributes(answer_params)

      if @answer.save
        flash[:notice] = 'The post has been updated'
        redirect_to learn_course_question_path(@question.course, @question)
        return
      end
      respond_with(@question)
    end

    def destroy
      unauthorized! if cannot? :destroy, @answer

      if @answer.user_id == current_user.id
        reason = 'Original poster\'s choice'
        content = '-- Deleted content --'
      else
        reason = 'Due to moderation reasons'
        # Leave content as proof
        content = @answer.content
      end


      if @answer.update_attributes({status: 'deleted', content: content, reason: reason})
        flash[:notice] = "Successfully deleted the post"
      else
        flash[:alert] = "Could not delete the post"
      end

      redirect_to learn_course_question_path(@question.course, @question)
    end

    protected
      def answer_params
        fields = [:content]

        if can?(:moderate, @question.answers.build)
          fields << :reason
          fields << :status
        end

        params.require(:learn_answer).permit(fields)
      end

      def load_question
        @question = Question.find(params[:question_id])
        @course = @question.course
      end

      def load_answer
        @answer = Answer.find(params[:id])
      end
  end
end
