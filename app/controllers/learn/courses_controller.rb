require 'learn/application_controller'

module Learn
  class CoursesController < ApplicationController

    before_action :load_course, except: [:index, :search, :create, :new, :joined, :questions]
    before_action :load_courses, only: [:index, :search, :joined]
    load_and_authorize_resource # except: [:index, :search, :joined]
    before_action :load_join_courses_questions, only: [:questions]
    before_action :load_adverts, except: [:create, :update, :destroy, :join, :leave]

    # load_and_authorize_resource

    respond_to :html

    def new
      @course = Course.new(name: nil)
    end

    def create
      @course = Course.new(course_params)
      @course.institution_website = current_user.profile.institution_website
      if @course.save
        flash[:notice] = "The course has been created"
      end

      respond_with(@course)
    end

    def show
      @questions = @course.questions.order(created_at: :DESC)
    end

    def edit
    end

    def update
      # @course = Course.find(params[:id]) # Hack to get around identity_cache's expiry problems
      @course.assign_attributes(course_params)
      if @course.save
        flash[:notice] = "Course updated successfully"
      end
      respond_with(@course)
    end

    def destroy
      @course = Course.find(params[:id])
      if @course.destroy
        flash[:notice] = "Successfully deleted the course"
      end
      redirect_back(learn_courses_path)
    end

    def search
    end

    def join
      
      if @course.attendees.where(user_id: current_user.id).limit(1).first.present?
        flash[:alert] = "You are already part of #{@course.code.upcase}"
      else

        if @course.attendees.create(user_id: current_user.id)
          flash[:notice] = "You have joined #{@course.code.upcase}"
        else
          flash[:alert] = "We unable to add you to the course at the moment, report this if it persists"
        end
      end

      return redirect_back(@course, "course_#{@course.id}")
    end

    def leave
      attendee = @course.attendees.where(user_id: current_user.id).limit(1).first

      if attendee.nil?
        flash[:alert] = "You are not part of the module"
      else
        if attendee.destroy
          flash[:notice] = "You have left this course"
        else
          flash[:alert] = "We were unable to remove you from this course at the moment, report this if it persists"
        end
      end

      return redirect_back(@course, "course_#{@course.id}")
    end

    def joined
      if params[:user_id].present?
        @user = User.find(params[:user_id])
      end
       
      @user ||= current_user
      @courses = @courses.joins(:attendees).where('learn_attendees.user_id=?', @user.id).references(:learn_attendees)

    end

    def questions
      if !Learn::Attendee.where(user_id: current_user.id).any?
        flash[:notice] = "You have to join a course first"
        redirect_to learn_courses_path
        return
      end
    end

    protected

      def load_course
        @course = Learn::ApplicationController.get_course(current_user, params, :internal_code)
      end

      def course_params
        is_edit = ['edit', 'update'].include?(action_name)

        fields = [ :name ]

        if (!is_edit || current_user.is_admin?) 
          fields << :code
        end

        params.require(:learn_course).permit(fields)
      end

      def load_courses
        @courses = Course.where(institution_website: current_user.profile.institution_website).order(:internal_code)

        # if params[:user_filter].present?
        #   @courses = @courses.where(user_id: params[:user_filter].to_i)
        # end

        @courses = @courses.page(params[:page]).per(40)
        @courses
      end

      def load_join_courses_questions

        @questions = current_user.all_course_questions(params[:page])

      end

  end
end
