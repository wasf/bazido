module Learn
  class ApplicationController < ::ApplicationController
    before_action :authenticate_user!

    def new
    end

    def self.get_course(current_user, params_array, field_id)
      if params_array[field_id] =~ /\A[0-9]+\z/
        course = Course.find(params_array[field_id])
        redirect_to course
      else
        course = Course.where(internal_code: params_array[field_id].downcase, institution_website: current_user.profile.institution_website).limit(1).first
        raise ActiveRecord::RecordNotFound if course.nil?
      end
      course
    end

    protected


      def follow_post(post, user)
        
      end


  end
end
