require 'learn/application_controller'

module Learn
  class QuestionsController < ApplicationController
    before_action :load_course
    before_action :load_question, except: [:new, :create, :index, :search]
    before_action :load_questions, only: [:index, :search]
    load_and_authorize_resource through: :course
    before_action :load_adverts, except: [:create, :update, :destroy]

    respond_to :html

    def index
    end

    def show
      @answer = Answer.new(question: @question, user: current_user)
    end

    def new
      @question = @course.questions.build(user_id: current_user.id)
    end

    def create
      @question = @course.questions.build(question_params)
      @question.user_id = current_user.id

      if @question.save
        flash[:notice] = 'Your question has been posted successfully'
        redirect_to learn_course_question_path(@course, @question)
      else
        render action: :new
      end

    end

    def edit
    end

    def update
      @question.assign_attributes(question_params)

      if @question.save
        flash[:notice] = 'The question was updated successfully'
        redirect_to learn_course_question_path(@course, @question)
      else
        render action: :edit
      end

    end

    def destroy
      if @question.destroy
        flash[:notice] = "Successfully deleted the question"
        redirect_to learn_course_questions_path(@course)
      else

        return redirect_back(learn_course_question_path(@course, @Question))
      end
    end

    def search
    end

    protected

      def load_course
        @course = Learn::ApplicationController.get_course(current_user, params, :course_internal_code)
      end

      def question_params
        fields = [:title, :content, :category]
        if can?(:moderate, @course.questions.build)
          fields << :status
          fields << :reason
        end

        params.require(:learn_question).permit(fields)
      end

      def load_question
        @question = Question.find(params[:id])
      end

      def load_questions
        @questions = @course.questions.order(created_at: :DESC).page(params[:page])
        if params[:category].present?
          @questions = @questions.where(category: params[:category])
        end
        @questions
      end
  end
end
