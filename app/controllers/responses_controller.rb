class ResponsesController < ApplicationController

  before_action :authenticate_user!
  before_action :load_resources

  respond_to :html

  def create
    authorize!(:respond, @message)

    @response = @message.responses.build(response_params)
    @response.user_id = current_user.id
    
    if @response.save
      @message.touch
      @message.watchers.where(read: true).where(status: 'open').update_all(read: false)
      flash[:notice] = "Your message has been sent"
    end

    respond_with(@message)
  end

  protected

    def response_params
      params.require(:response).permit(:content)
    end

    def load_resources
      @message = Message.find(params[:message_id])
    end

end
