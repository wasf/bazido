class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # before_action :load_adverts, except: [:create, :update, :destroy]

  helper_method :get_refreshable_id

  rescue_from CanCan::AccessDenied do |exception|
   render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false)
  end

  # A hook in your ApplicationController
  def authorize
    if current_user.profile.role == 'admin'
      Rack::MiniProfiler.authorize_request
    end
  end

    # def self.included(base)
    #   base.rescue_from Errors::NotFound do |e|
    #     render "public/404", :status => 404
    #   end
    #   base.rescue_from Errors::UnprocessableEntity do |e|
    #     render "public/422", :status => 422
    #   end
    #   base.rescue_from Errors::InternalServerError do |e|
    #     render "public/502", :status => 500
    #   end
    # end
  
  
  protected
    def create_watcher(watchable, user,application_space)
      watcher = watchable.watchers.create(user_id: user.id, checked_at: Time.now, status: 'open', application_space: application_space, read: true)
      watcher.save
    end

    def reset_watcher(watchable, user, cache_count_name=nil)

      watchable.watchers.where(user_id: user.id).where(read: false).update_all(read: true)
      if cache_count_name
        Rails.cache.delete([user, cache_count_name])
      end

    end

    def update_watcher(watchable, user, application_space, cache_count_name)

        watcher = watchable.watchers.where(user_id: user.id).where(status: 'open').first
        if watcher.nil?
          watcher = watchable.watchers.build(user_id: user.id, status: 'open', application_space: application_space, read: true, checked_at: Time.now)
        end

        watcher.read = true
        watcher.save

        # Let all other users know they have not read this post
        watchable.watchers.where(status: 'open').where(read: true).update_all(read: false)

        # Reset the memcached rows for these users
          watchable.watchers.where(status: 'open').each do |u|
            Rails.cache.delete([u.user, cache_count_name])
          end
          # Rails.cache.delete()
    end

    def get_refreshable_id(object)
      object.class.name.split('::').push(object.id).join('_').downcase
    end

    def redirect_back(fallback, anchor = nil)

      begin

        # Add an anchor so the browser can jump back to it
        if env['HTTP_REFERER'].present?

          if anchor != nil
            referer = env['HTTP_REFERER'].split('#')[0..1]
            referer[1] = anchor
            env['HTTP_REFERER'] = referer.join('#')
          end
          redirect_to env['HTTP_REFERER'], :only_path => true
        else
          redirect_to fallback
        end

      rescue ActionController::RedirectBackError
        redirect_to fallback
      end
    end

    def load_adverts # (object)
      model_name = controller_name.singularize
      object = instance_variable_get("@#{model_name}")
      @loaded_advertisement = Rails.cache.fetch([object, 'advertisements'], expires_in: 2.minutes) do
        Advertise::Advertisement.advertise(current_user, object)
      end

      @loaded_sponsor = Rails.cache.fetch([object, 'sponsors'], expires_in: 2.minutes) do
        Advertise::Sponsor.advertise(current_user, object)
      # raise @loaded_sponsor
      end

    end

end
