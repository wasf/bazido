class ProfilesController < ApplicationController
  respond_to :json
  before_action :authenticate_user!
  # load_and_authorize!

  # For use with the best_in_place gem
  def update
    # if 
    user = @profile.user
    unauthorized! if cannot?(:update, user)
    # mongo_user = Mongo::User.find(user.id)
    # mongo_user.update_attributes(user.to_mongo)
    # mongo_user.save
    respond_with(@profile)
  end

  private
    
    def profile_params
      params.require(:profile).permit(:avatar)
    end

end
