require_dependency "market/application_controller"

module Market
  class AccommodationsController < ApplicationController

    before_action :load_accommodation, :except => [:new, :create, :index, :search]
    before_action :load_accommodations, :only => [:index]
    load_and_authorize_resource# class: Accommodation
    before_action :load_adverts, except: [:create, :update, :destroy]
    # load_and_authorize_resource# class: Accommodation
    respond_to :html

    def index
    end

    def show
      # raise @loaded_sponsor
    end

    def new

      @accommodation = Accommodation.new
      @accommodation.build_refresh(user_id: current_user.id)
      
      # Create 4 images for the refresh for users on mobile
      2.times do
        @accommodation.refresh.images.build(:id => nil)
      end

      @accommodation.build_necessity
    end

    def create
      @accommodation = Accommodation.new(accommodation_params)
      @accommodation.refresh.user_id = current_user.id
      #TODO: Set expires times
      @accommodation.refresh.next_allowed = Refresh.first_cycle(1/7.0)
      @accommodation.refresh.last_allowed = Time.now
      @accommodation.refresh.expires = Refresh.second_cycle
      @accommodation.refresh.institution_website = current_user.profile.institution_website

      if @accommodation.save
        @accommodation.__elasticsearch__.index_document
        flash[:notice] = 'Accommodation post has been created successfully' 
        remind_about_refreshing
      end

      respond_with(@accommodation)
    end

    def edit
    end

    def update

      @accommodation = Accommodation.find(params[:id])
      @accommodation.assign_attributes(accommodation_params)
      if @accommodation.save
        @accommodation.__elasticsearch__.index_document
        flash[:notice] = "Successfully updated the post"
        remind_about_refreshing
      end
      respond_with(@accommodation)
    end

    def destroy

      @accommodation == Accommodation.find(params[:id])
      if @accommodation.destroy
        @accomodation.__elasticsearch__.index_document
        flash[:notice] = "Post has been succesfully destroyed"
      else
        flash[:alert] = "Failed to delete this post"
      end

      # respond_with(@accommodation)
      redirect_to market_accommodations_path

    end

    def search
      if params[:query].present? && params[:query].strip != ''
        query = params[:query]
        @accommodations = Accommodation.search(query).records.includes(:refresh).where(:market_refreshes => {:is_public => true}).apply_filter(params[:filter]).college(false, current_user.profile.institution_website).page(params[:page]).references(:market_refreshes)
        # raise @accommodations
      else
        # @accommodations = []
        load_accommodations
      end
    end

    private

      def accommodation_params
        params.require(:market_accommodation).permit(inject_refresh_params(:title, :description, :deposit, :rent, :location, :first_payment, :occupation_date, :necessity_attributes => [:id, :furnished, :shared, :sharing, :rooms, :electricity, :water, :parking, :security, :laundry, :floor_area]))
      end

      
      # Take advantage of caching below

      def load_accommodation
        @accommodation = Market::Accommodation.includes(:refresh, :necessity).find_by_id(params[:id])
      end

      def load_accommodations
        @accommodations = Accommodation.apply_filter(params[:filter]).college(false, current_user.profile.institution_website).all.page(params[:page]) #.most_recent(current_user.profile.institution_website)
        # @accommodations = Market::Accommodation.most_recent(current_user.profile.institution_website)
      end

  end
end
