require_dependency "market/application_controller"

module Market
  class ContactsController < ApplicationController

    before_action :load_adverts, except: [:create, :update, :destroy]
    respond_to :html
    load_and_authorize_resource

    def index
      @contacts = Contact.belonging_to_user(current_user)
    end

    def edit
      @contact = Contact.new # Prevents in the form from having the @contact data
      @contacts = Contact.belonging_to_user(current_user)
    end

    def update
      @contact_to_update = Contact.find(params[:id])

      @contact_to_update.assign_attributes(contact_params)
      if @contact_to_update.save
        flash[:notice] = "Successfuly updated your contact"
        anchor = "contact_#{@contact_to_update.id}"
        return redirect_to market_contacts_path(anchor: anchor)
      else
        @contact = Contact.new # Prevents in the form from having the @contact data
        @contacts = Contact.belonging_to_user(current_user)
        render :action => :index
      end

    end

    def create
      @contact = Contact.new(contact_params)
      @contact.user_id = current_user.id

      if @contact.save
        flash[:notice] = "Contact added successfully"
        return redirect_to market_contacts_path
      else
        # flash[:alert] = "The form has errors"
        @contacts = Contact.belonging_to_user(current_user)
        render :action => :index
      end

    end

    def destroy
      
      @contact = Contact.find(params[:id])
      if @contact.destroy
        flash[:notice] = "Contact destroyed succesfully"
      else
        flash[:alert] = "Failed to delete contact due to technical error. Report it to us if it persists"
      end

      redirect_to market_contacts_path

    end

    private
      
      def contact_params
        params.require(:market_contact).permit(:private_name, :name, :content)
      end

  end
end
