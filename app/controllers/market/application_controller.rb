module Market
  class ApplicationController < ::ApplicationController #ActionController::Base

    # TODO: Allow users to view the index and show but only if they specify the university's web address
    before_action :authenticate_user!, :except => [:show]

    protected
      # def current_ability 
      #  @ability = Ability.new(current_user)
      # end

      def inject_refresh_params(*array_to_inject_to)

        refresh_hash = [ refresh_attributes: [:status, :is_public, :contact_ids => [], :images_attributes => [:id, :_destroy, :display]] ]

        array_to_inject_to.concat(refresh_hash)

      end

      def remind_about_refreshing
        if current_user.present? && current_user.profile.created_at > 2.weeks.ago
          object_name = controller_name.singularize
          flash[:notice] << ". Don't forget to bump whenever the link appears to show the availability of this #{object_name}."
        end
      end

  end
end
