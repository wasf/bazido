require_dependency "market/application_controller"

module Market
  class RefreshesController < ApplicationController

    before_action :load_refresh
    authorize_resource

    respond_to :html, :json

    def refresh
      # TODO: Limit how frequently users can refresh their stuff
      if @refresh #.next_allowed < Time.now

        post_name = ''

        case @refresh.refreshable.class.name
        when "Market::Accommodation"
          # Refresh the accommodation accordingly
          # flash[:notice] = "It is a accommodation"
          @refresh.last_allowed = Time.now
          @refresh.next_allowed = 16.hours.from_now
          @refresh.expires = Refresh.second_cycle
          post_name = 'accommodation'
          
        when "Market::Book"
          # Refresh the book accordingly
          # flash[:notice] = "It is a book. Implement it's refresh"
          @refresh.last_allowed = Time.now
          @refresh.next_allowed = 6.hours.from_now
          @refresh.expires = Refresh.first_cycle((6.0/7.0))
          post_name = 'book'

        else
          # Tell the user that such action is not valid
          flash[:alert] = "This looks like a post that can't be bumped if it exists. We can't find it in our database."
        end


      else
        flash[:alert] = "You can't bump this post at this time, please wait for the next cycle"
      end

      # Save the post only if it has been changed and replace messages if needed
      if post_name.present?

        @refresh.last_allowed = Time.now

        if @refresh.save
          flash[:notice] = "The #{post_name} post has been bumped"
        else
          flash[:alert] = "The #{post_name} post could not be bumped due to technical error, report it if it persists"
        end
      end

      the_dom_id = get_refreshable_id(@refresh.refreshable)
      redirect_back(@refresh.refreshable, the_dom_id.downcase)
    end

    def update

      # if @refresh.update_attributes(refresh_params)
      #   flash[:notice] = "Succesfully updated the post"
      # end
      # raise params[:market_refresh][:status]
      @refresh = Refresh.find(params[:id])
      @refresh.assign_attributes(refresh_params)
      @refresh.save

      respond_with(@refresh)

    end

    private

      def load_refresh
        # TODO: Use Dalli to load
        @refresh = Refresh.find(params[:id])
      end

      def refresh_params
        # These are the ones that can be updated, perhaps with the best_in_place gem
        params.require(:market_refresh).permit(:status, :is_public)
      end

  end
end
