require_dependency "market/application_controller"

module Market
  class BooksController < ApplicationController

    before_action :load_book, :except => [:new, :create, :index, :search]
    before_action :load_books, :only => [:index]
    load_and_authorize_resource
    before_action :load_adverts, except: [:create, :update, :destroy]

    respond_to :html

    def index
    end

    def show
    end

    def new

      @book = Book.new
      @book.build_refresh

      #There can be a picture of at least 1 book
      @book.refresh.images.build

    end

    def create
      @book = Book.new(book_params)
      @book.refresh.user_id = current_user.id
      # TODO: Check the validity of the expire times
      @book.refresh.next_allowed = Refresh.first_cycle(1/7.0)
      @book.refresh.last_allowed = Time.now
      @book.refresh.expires = Refresh.first_cycle(6.0/7.0)
      @book.refresh.institution_website = current_user.profile.institution_website

      if @book.save
        @book.__elasticsearch__.index_document
        # Create the object and store it in mongo
        # mongo_book = Mongo::Book.new(@book.to_mongo)
        # mongo_book.upsert
        flash[:notice] = 'Book post has been created successfully'
        remind_about_refreshing
      end

      respond_with(@book)
    end

    def edit
    end

    def update
      
      @book = Book.find(params[:id])
      @book.assign_attributes(book_params)
      if @book.save
        @book.__elasticsearch__.index_document
        # mongo_book = Mongo::Book.new(@book.to_mongo)
        # mongo_book.upsert
        flash[:notice] = 'Book post has been updated successfully'
        remind_about_refreshing
      end

      respond_with(@book)
    end

    def destroy

      @book = Book.find(params[:id])
      if @book.destroy
        @book.__elasticsearch__.index_document
        # begin
          # mongo_book = Mongo::Book.find(@book.id)
          # mongo_book.destroy
        # rescue Mongoid::
        flash[:notice] = 'The book post was successfully deleted'
      else
        flash[:alert] = 'Could not delete the book post due to technical errors'
      end

      # respond_with(@book)
      redirect_to market_books_path
    end

    def search
      if params[:query].present? && params[:query].strip != ''
        # raise params
        # if params[:field] != 'course' # There are only two at the moment
          query = params[:query]
          @books = Book.search(query).records.includes(:refresh).where(:market_refreshes => {:is_public => true}).references(:market_refreshes).apply_filter(params[:filter]).college(false, current_user.profile.institution_website).page(params[:page])
        # else
        #   # Courses is the only other option
        #   courses_array = Book.split_courses_to_mongo_format(params[:query])          # The courses have now been split. Perform query on mongodb
        #   if courses_array.length > 0
        #     book_ids = Mongo::Book.where(:courses.in => courses_array).pluck(:_id)
        #     @books = Book::where(id: book_ids).apply_filter(params[:filter]).page(params[:page])
        #     @books
        #   else
        #     # @books = []
        #     load_books
        #   end
        # end
      else
        # @books = []
        load_books
      end
    end

    private

      def book_params
        params.require(:market_book).permit(inject_refresh_params(:title, :author, :edition, :isbn, :courses, :condition, :age, :price, :comments))
      end

      def load_book
        # Sweet spot to load a book from cache
        @book = Book.find_by_id(params[:id])
      end

      def load_books
        # TODO: Load books but take filter parameters into account
        @books = Book.apply_filter(params[:filter]).college(false, current_user.profile.institution_website).all.page(params[:page]) #.most_recent(current_user.profile.institution_website)
      end

      # Creates a book for mongodb which is used for some queries

  end
end
