require_dependency "market/application_controller"

module Market
  class IndexController < ApplicationController

    before_action :load_index
    before_action :load_adverts, except: [:create, :update, :destroy]
    # authorize_resource

    def index
      # books = Mongo::Book.filter_by_course#([1])
    end

    private

      def load_index
        # refreshes
        # @accommodations = Accommodation.freshest.all
        # @books = Book.freshest.all
        @refreshes = Refresh.apply_filter(params[:filter], current_user, true).college(true, current_user.profile.institution_website).all.page(params[:page])
        @refreshables = @refreshes.collect do |refresh|
          refresh.refreshable
        end
      end
  end
end
