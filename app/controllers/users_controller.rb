class UsersController < ApplicationController
  # x = []
  # raise x
  cache_sweeper :user_sweeper, only: [:create, :update, :destroy]
  before_action :authenticate_user!, except: [:show, :market]
  before_action :load_user
  authorize_resource
  before_action :load_refreshes
  # before_action :load_adverts, except: [:create, :update, :destroy]
  respond_to :html
  layout '/layouts/two_columns'

  def show
    
    # @user = User.find(params[:id])
    @refreshes = load_refreshes
    @refreshables = extract_refreshables(@refreshes)
    respond_with(@user)

  end

  def index
    # profiles = Profile.all(:institution_website => current_user.profile.institution_website)
  end

  def edit
    respond_with(@user)
  end

  def update

    @user = User.find(params[:id])
    @user.assign_attributes(user_params)
    if @user.save
      # mongo_user = Mongo::User.new(@user.to_mongo)
      # mongo_user.upsert
      flash[:notice] = I18n.translate('controller.users.update.success')
    end

    respond_with(@user)

  end

  def market
    @refreshes = load_refreshes(30)
    @refreshables = extract_refreshables(@refreshes)
  end

  private

    def load_refreshes(limit = nil)
        refreshes = Market::Refresh.apply_filter(params[:filter], nil, true).where(user_id: @user.id)

        if limit.nil?
          refreshes = refreshes.page(params[:page]).per(5)
        else
          refreshes = refreshes.page(params[:page]).per(limit)
        end

        refreshes = refreshes.where(is_public: true) unless can? :update, @user
        # raise @user
        refreshes
    end

    def extract_refreshables(refreshes)
      is_owner = true if can? :update,  @user
      refreshables = refreshes.collect do |refresh|
        refreshable = Rails.cache.fetch("/market/refreshes/#{refresh.id}/refreshable/#{is_owner}") do
          refresh.refreshable
        end
        refreshable
      end
      refreshables
    end

    def load_user
      @user = Rails.cache.fetch(['users', params[:id]]) do
        User.find(params[:id])
      end
    end

    def user_params
      params.require(:user).permit(:profile_attributes => [:original_institution_website, :full_name, :username, :gender, :bio, :website, :avatar, :location, :academics_attributes => [ :id, :_destroy, :institution, :department, :code, :name, :commence, :graduation, :comment ], :employments_attributes =>[:id, :_destroy, :company, :position, :location, :current_employer, :start_date, :end_date, :description]])
    end

end
