require_dependency "forum/application_controller"

module Forum
  class CommentsController < ApplicationController

    before_action :load_resources

    def create
      unauthorized! if cannot?(:comment, @thread)
      unauthorized! if cannot?(:create, Comment.new(thread: @thread))

      @comment = @thread.comments.build(comment_params)
      @comment.user_id = current_user.id
      @comment.status = 'visible' if @comment.status.nil?
      
      if @comment.save
        @thread.touch

        # Create or edit watcher for this current user
          update_watcher(@thread, current_user, 'Forum', 'forum_threads_notifications_count')
          # watcher = @thread.watchers.where(user_id: current_user.id).where(status: 'open').first
          # if watcher.nil?
          #   watcher = @thread.watchers.build(user_id: current_user.id, status: 'open', application_space: 'Forum', read: true, checked_at: Time.now)
          # end

          # watcher.read = true
          # watcher.save

        # ========================================
        # Let all other interested users know they have not read this new comment
          # @thread.watchers.where(status: 'open').where(read: true).update_all(read: false)
        # ========================================


        flash[:notice] = "Successfully responded to the post"
        redirect_to forum_thread_path(@thread, page: @thread.comments.page(params[:page]).num_pages, anchor: @comment.id)
      else
        @comments = @thread.comments
        flash[:forum_comment_content] = @comment.content
        flash[:forum_comment_errors] = @comment.errors
        redirect_to @thread
      end

    end

    def edit
    end

    def update
    end

    def destroy
      unauthorized! if cannot? :destroy, @comment

      if @comment.user_id == current_user.id
        reason = 'Original poster\'s choice'
        content = '-- Deleted content --'
      else
        reason = 'Due to moderation reasons'
        # Leave content as proof
        content = @comment.content
      end


      if @comment.update_attributes({status: 'deleted', content: content, reason: reason})
        flash[:notice] = "Successfully deleted the post"
      else
        flash[:alert] = "Could not delete the post"
      end
      redirect_to @thread
    end

    protected

      def load_resources
        @thread = Forum::Thread.page(params[:page]).find(params[:thread_id])

          @comment = Forum::Comment.find_by_id(params[:id])# @thread.comments.where(id: params[:thread_id]).first
      end

      def comment_params
        param_array = [:content]

        if can? :moderate, @thread
          param_array << :status
          param_array << :reason
        end

        params.require(:forum_comment).permit(param_array)
      end
  end
end
