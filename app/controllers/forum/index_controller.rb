require_dependency "forum/application_controller"

module Forum
  class IndexController < ApplicationController

    before_action :load_adverts, except: [:create, :update, :destroy]

    def index
    end
  end
end
