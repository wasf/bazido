require_dependency "forum/application_controller"

# Remeber to always use Forum::Thread else you'll be calling Ruby's thread


module Forum
  class ThreadsController < ApplicationController

    before_action :load_thread, except: [:new, :create, :index, :watching]
    before_action :load_threads, only: [:index, :watching]
    before_action :load_adverts, except: [:create, :update, :destroy]
    load_and_authorize_resource

    respond_to :html

    def new
      @thread = Forum::Thread.new(category: params[:category], status: 'open')
    end

    def create
      @thread = Forum::Thread.new(thread_params)

      @thread.user_id = current_user.id
      @thread.institution_website = current_user.profile.institution_website

      if @thread.save
        create_watcher(@thread, current_user, 'Forum')
        # watcher = @thread.watchers.create(user_id: current_user.id, checked_at: Time.now, status: 'open', application_space: 'Forum', read: true)
        # watcher.save
        flash[:notice] = 'Post created succesfully'
      end

      respond_with(@thread)
    end

    def edit
    end

    def update
      @thread = Forum::Thread.find(params[:id])
      @thread.assign_attributes(thread_params)
      if @thread.save
        flash[:notice] = 'Successfully updated post'
      end

      respond_with(@thread)
    end

    def show
      @comment = Forum::Comment.new(status: :visible, content: flash[:forum_comment_content])
      @comments = @thread.comments # .page(params[:page])

      # Mark the user as having read this thread
      reset_watcher(@thread, current_user, 'forum_threads_notifications_count')
      # @thread.watchers.where(user_id: current_user.id).where(read: false).update_all(read: true)
      # Rails.cache.delete([current_user, 'forum_threads_notifications_count'])

      # Copy possible errors into the comment if from another comment
      if flash[:forum_comment_errors].present?
        flash[:forum_comment_errors].each do |key, value|
          if value.is_a? Array

            value.each do |error|
              @comment.errors.add(key, error)
            end

          else
            @comment.errors.add(key, value)
          end
        end
      end
    end

    def destroy
      if @thread.destroy
        flash[:notice] = "Successfully deleted what you said"
      else
        flash[:alert] = "Could not delete what you said due to technical error"
      end

      redirect_to forum_threads_path
    end

    def index
    end

    def watching
      @threads = current_user.watching_forum_threads(@threads) # @threads.for_user(current_user).page(params[:page])
      render action: :index
    end

    protected

      def thread_params
        param_array = [:title, :content, :category]

        if can? :moderate, @thread
          param_array << :status
          param_array << :reason
        end

        params.require(:forum_thread).permit(param_array)
      end

      def load_thread
          @thread = Forum::Thread.fetch(params[:id])
      end

      def load_threads
        @threads = Forum::Thread.cached_for_institution(current_user.profile.institution_website, params[:category], params[:page]) 

        if can? :moderate, @thread
          if params[:status].present?
            @threads = @threads.where(status: params[:status])
          end
        end

        @threads
      end
  end
end
