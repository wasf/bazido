require_dependency "advertise/application_controller"

module Advertise
  class SponsorsController < ApplicationController

    before_action :load_sponsor, except: [:new, :create, :index]
    before_action :load_sponsors, only: [:index]
    load_and_authorize_resource
    respond_to :html

    def new
      @sponsor = Sponsor.new
      1.times do
        @sponsor.displays.build(id: nil, show_at: 1.hour.from_now.utc)
      end
    end

    def create
      @sponsor = Sponsor.new(sponsor_params)
      @sponsor.user = current_user
      @sponsor.paid = false

      # @sponsor.institution_website = current_user.profile.institution_website
      if @sponsor.save
        # Charge all the succesful displays
        sponsor_prices(@sponsor)

        flash[:notice] = 'Sponsor created successfully'
      end

      respond_with(@sponsor)
    end

    def show
    end

    def index
    end

    def edit
    end

    def update
      @sponsor.assign_attributes(sponsor_params)
      if @sponsor.save
        sponsor_prices(@sponsor)
        flash[:notice] = 'Sponsor updated successfully'
      end
      respond_with(@sponsor)
    end

    def destroy
      if @sponsor.destroy
        flash[:notice] = 'Successfully deleted the sponsor post'
      else
        flash[:notice] = 'Could not delete the sponsors post due to technical errors. Please try again later.'
      end

      # respond_with(@sponsor)
      redirect_to advertise_sponsors_path
    end

    def activate
      if set_as_paid(@sponsor)
        flash[:notice] = 'Successfully activated an sponsor'
      else
        flash[:notice] = 'Could not activate the sponsor. Check your balance first and report it to us if it is an error'
      end

      redirect_back(advertise_sponsors_path)
    end

    protected

      def sponsor_params
        params.require(:advertise_sponsor).permit(:title, :link, :display, displays_attributes: display_attributes)
      end

      def load_sponsor
        @sponsor = Sponsor.includes(:displays).find_by_id(params[:id])
      end

      def load_sponsors
        @sponsors = Sponsor.includes(:displays).where(user_id: current_user.id).order(created_at: :DESC).all.page(params[:page])
      end
  end
end
