require_dependency "advertise/application_controller"

module Advertise
  class AdvertisementsController < ApplicationController

    before_action :load_advertisement, except: [:new, :create, :index]
    before_action :load_advertisements, only: [:index]
    load_and_authorize_resource
    respond_to :html

    def new
      @advertisement = Advertisement.new
      1.times do
        @advertisement.displays.build(id: nil, show_at: 1.hour.from_now.utc)
      end
    end

    def create
      @advertisement = Advertisement.new(advertisement_params)
      @advertisement.user = current_user
      @advertisement.paid = false

      # @advertisement.institution_website = current_user.profile.institution_website
      if @advertisement.save
        # Charge all the succesful displays
        advertisement_prices(@advertisement)
        flash[:notice] = 'Advertisement created successfully'
      end

      respond_with(@advertisement)
    end

    def show
    end

    def index
    end

    def edit
    end

    def update
      @advertisement.assign_attributes(advertisement_params)
      if @advertisement.save
        advertisement_prices(@advertisement)
        
        flash[:notice] = 'Advertisement updated successfully'
      end
      respond_with(@advertisement)
    end

    def destroy
      if @advertisement.destroy
        flash[:notice] = 'Successfully deleted the advertisement post'
      else
        flash[:notice] = 'Could not delete the advertisements post due to technical errors. Please try again later.'
      end

      # respond_with(@advertisement)
      redirect_to advertise_advertisements_path
    end

    def activate
      if set_as_paid(@advertisement)
        flash[:notice] = 'Successfully activated an advertisement'
      else
        flash[:notice] = 'Could not activate the advertisement. Check your balance first and report it to us if it is an error'
      end

      redirect_back(advertise_advertisements_path)
    end

    protected

      def advertisement_params
        params.require(:advertise_advertisement).permit(:title, :link, :display, displays_attributes: display_attributes)
      end

      def load_advertisement
        @advertisement = Advertisement.find(params[:id])
        # @advertisement = Advertisement.includes(:displays).find_by_id(params[:id])
      end

      def load_advertisements
        advertisements = Advertisement.cached_for_user(current_user)
        @advertisements = advertisements.page(params[:page])
        @advertisements
      end
  end
end
