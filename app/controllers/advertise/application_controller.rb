# require_dependency "advertise/application_controller"

module Advertise
  class ApplicationController < ::ApplicationController #ActionController::Base

    before_action :authenticate_user!, except: [:pricing]

    protected
      # def current_ability 
      #  @ability = Ability.new(current_user)
      # end

      def set_as_paid(object)
        object.paid = true
        object.save
      end

      def advertisement_prices(advertisable)
        object = advertisable.displays
        if current_user.profile.role != 'admin'
          object = object.where('show_at > ?', 1.hour.from_now)
        end

        costs = Display.advertisement_prices
       set_prices(object, costs) 
      end

      def sponsor_prices(advertisable)
        object = advertisable.displays
        if current_user.profile.role != 'admin'
          object = object.displays.where('show_at > ?', Time.now.to_date)
        end
        costs = Display.sponsor_prices
        set_prices(object, costs)
      end

      def display_attributes
        dp_attributes = [:id, :institution_website, :show_at, :_destroy, :consecutive_days]
        if current_user.profile.role == 'admin'
          dp_attributes << :free
        end
        dp_attributes

      end

      private

        def set_prices(displays, costs_array)

          displays.where('consecutive_days < 7').update_all(cost: costs_array[0])

          displays.where('consecutive_days > 6 AND consecutive_days < 14').update_all(cost: costs_array[1])

          displays.where('consecutive_days > 13 AND consecutive_days < 28').update_all(cost: costs_array[2])

          displays.where('consecutive_days > 27').update_all(cost: costs_array[3])

        end

  end
end
