require_dependency "advertise/application_controller"

module Advertise
  class PaymentsController < ApplicationController
    
    protect_from_forgery :except => [:create]
    
    before_action :authenticate_user!, except: [:create]

    before_action :load_payments, only: [:index]
    load_and_authorize_resource except: :create

    def index

    end

    def new
      # @payment = Payment.new
      # @transaction = Transaction.new
      flash[:alert] = "That functionality is not in use"
      redirect_to advertise_payments_path
    end

    def create

      if valid_paypal_request
        custom_array = params[:custom].split(',')
        @user = User.find(custom_array[0])

        # Mark the user as having a payment
        if @user.profile.has_payment == false
          @user.profile.has_payment = true
          @user.save
        end

        @payment = Payment.new(:params => params, invoice: params[:ipn_track_id], transaction_id: params[:txn_id], refunded: false, date_paid: Time.now, amount: params[:mc_gross], user_id: @user.id)

        # TODO: Report it when 
        @payment.save


      end

      render nothing: true

    end

    private
      def transaction_params
        params.require(:transaction).permit(:amount, :first_name, :last_name, :card_number, :month, :year, :verification_value, :card_type)
      end

      def load_payments
        @user ||= current_user
        @payments = @user.payments.order(created_at: :DESC).page(params[:page]) #Payment.where(user_id: @user.id)
      end

      def valid_paypal_request
        paypal_emails = ['thabang.biz@gmail.com']

        paypal_emails.include?(params[:receiver_email]) &&
        params[:payment_status] == 'Completed' &&
        params[:mc_currency] == 'USD' &&
        Advertise::Payment.find_by_transaction_id(params[:txn_id]) == nil &&
        validate_IPN_notification
      end

      protected
        def validate_IPN_notification

          raw = request.raw_post

          uri = URI.parse('https://www.paypal.com/cgi-bin/webscr?cmd=_notify-validate')
          http = Net::HTTP.new(uri.host, uri.port)
          http.open_timeout = 60
          http.read_timeout = 60
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          http.use_ssl = true
          response = http.post(uri.request_uri, raw).body

          if response == 'VERIFIED'
            return true
          else
            false
          end
        end

  end
end
