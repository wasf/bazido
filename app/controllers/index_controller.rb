class IndexController < ApplicationController
  def index
  end

  def terms
  end

  def redirect
    render layout: false
    if params[:url].nil?
      flash[:alert] = 'Could not redirect to a invalid web page'
      redirect_back(root_path)
    end
  end
end
