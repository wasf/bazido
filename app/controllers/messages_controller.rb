class MessagesController < ApplicationController

  before_action :authenticate_user!
  before_action :load_adverts, except: [:create, :index]
  before_action :load_message, except: [:index, :new, :create]
  # load_and_authorize_resource
  before_action :load_messages, only: [:index]

  respond_to :html

  def create
    
    @messageable = find_messageable
    authorize!(:message, @messageable)

    @message = @messageable.messages.build(message_params)
    @message.user_id = current_user.id

    the_response = @message.responses.build
    the_response.content = params[:message][:responses_attributes].first[1]['content']
    the_response.user_id = current_user.id

    # Leave these watchers for the time being. They are specific to messages
    watchers = [@messageable.owner_id, current_user.id]

    watchers.each do |watcher_id|
      watcher = @message.watchers.build(user_id: watcher_id, status: 'open', application_space: @application_space, checked_at: 1.day.ago)
      if watcher_id == current_user.id
        watcher.read = true
      else
        watcher.read = false
      end
    end

    if @message.save
      # Clear the cache for all messages from users
      flash[:notice] = "Your message has been sent"
    end

    respond_with(@message)

    
  end

  def new

    @messageable = find_messageable
    authorize!(:message, @messageable)

    @message = Message.new
    @message.responses.build

  end

  def index
    render layout: '/layouts/two_columns'
  end

  def show
    @messageable = @message.messageable
    authorize!(:read, @message)
    @message.watchers.where(user_id: current_user.id).where(read: false).limit(1).update_all(read: true)
    Rails.cache.delete([current_user, 'message_notifications_count'])
    # Clear cache used for message index
    Rails.cache.delete([@message, 'watcher',current_user.id])
    @message_notifications = current_user.message_notifications
    # raise @message
    @response = Response.new(message_id: @message.id)
  end
  
  protected

    def load_message
      @message = Message.includes(:user, :watchers).find(params[:id])
    end
    
    def load_messages
      # @messages = current_user.messages.includes(:responses).page(params[:page])

      statuses = ['open', 'closed']
      @messages = Message.cached_user_messages(current_user, params[:page], params[:message_filter], statuses).page(params[:page])
      @messages
    end

    def message_params
      params.require(:message).permit(:title)
    end

#     def response_params
#       params.require(:response).permit(:content)
#     end

    # Find the messageable object
    def find_messageable
      params.each do |name, value|
        if name =~ /(.+)_id$/
          resolved_namespaced_value = resolve_namespace($1)
          return resolved_namespaced_value.classify.constantize.fetch(value)
        end
      end

      nil
    end

    # Some models live in different namespaces. Fix that here
    def resolve_namespace(value)
      market_stuff = ['accommodation', 'book']
      
      if market_stuff.include?(value)
        @application_space = 'market'
        return "Market::#{value.humanize}"
      end

      @application_space = ''
      value.humanize
    end

end
