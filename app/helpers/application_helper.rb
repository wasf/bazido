# module NokogiriFilter
#   def remove_tags_and_preserve_content(*list)

#     xpath("//*").each do |element|
#       if list.include?(element.name)
#         element.children.reverse.each do |child|
#           child_clone = child.clone
#           element.add_next_sibling child_clone
#           child.unlink
#         end
#         element.unlink
#       end
#     end

#   end
# end

# class Nokogiri::XML::Element
#   include NokogiriFilter
# end

# class Nokogiri::XML::NodeSet
#   include NokogiriFilter
# end

module ApplicationHelper

  def bazido_code_format(text)
    html_text = h(text)

    # Github style code block hack without markdown
    # html_text = html_text.gsub(/[\r\n]+\/~~~[\r\n]+/, '<pre class="prettyprint">')
    # html_text = html_text.gsub(/[\r\n]+\\~~~[\r\n]*/, '</pre>')
    # new_text = html_text.gsub(%r{(/~~~)([^<]+)(\\~~~)}, '<pre class="prettyprint">\2</pre>')

    match_opener_count = html_text.scan(/&lt;code&gt;/m).size
    match_closer_count = html_text.scan(/&lt;\/code&gt;/m).size

    # Add openers where required.
    new_text = html_text.gsub('&lt;code&gt;', '<code><pre class="prettyprint">') if html_text =~ /&lt;\/code&gt;/
    new_text = new_text.gsub("&lt;/code&gt;", '</pre></code>') if new_text.present? && new_text =~ /<code><pre class="prettyprint">/

    close_counter = match_opener_count - match_closer_count
    new_text = html_text if new_text.nil?

    if close_counter > 0
      # Close all open code tags
      close_counter.times do
        new_text << '</pre></code>'
      end
    end

    # Remove <p> in pretty print
      new_text = "<div>#{new_text}</div>"
      nokogiri = Nokogiri::HTML(new_text)
      pres = nokogiri.xpath("//pre")

      # g = pres.clone

      if defined?(pres[0]) && pres[0].try(:name) == 'pre'
        pres.each do |pre|
          _remove_tags_and_preserve_content(pre)
        end
      elsif pres.try(:name) == 'pre'
          _remove_tags_and_preserve_content(pres)
      end

      # raise pres
    # ==========================

     nokogiri.xpath('//div').to_s.gsub(/(?:\n\r?|\r\n?){2,}/, "<br><br>").gsub(/(?:\n\r?|\r\n?)/, "<br>").html_safe
    # new_text.html_safe
    # simple_format(new_text.html_safe)

    # raise html_text
  end

  def bazido_preview_format(text, length=160)

      new_text = "<div>#{h(text)}</div>"
      nokogiri = Nokogiri::HTML(new_text)

      truncate(nokogiri.xpath('//div').text.split.join(" "), length: length)

  end

  def _remove_tags_and_preserve_content(pre)
    pre_text = pre.text.strip
    if pre.first
      pre.content = pre_text # .gsub(/(?:\n\r?|\r\n?)/, "<br>")
      pre.each {|element| element.unlink if element != pre.first}
    end
  end

  def greater_than_thousand(num)
    if num > 1000
      return '1000+'
    else
      return num
    end
  end

  def messages_label(user)

    messages_label = '<i class="fi-mail large"></i>' # 'Mesages'
    if user.nil?
      return messages_label
    end

    count = user.cached_message_notifications_count
    if count > 0
      "#{messages_label}<span class='notification-span'><span id='messages-sup'>#{greater_than_thousand(count)}</span></span>".html_safe
    else
      "#{messages_label}<span class='notification-span'><span id='messages-sup'></span></span>".html_safe
    end

  end

  def forum_threads_label(user, label_name='Forum')

    if user.nil?
      return '<i class="fi-comments large"></i>'.html_safe # label_name
    end

    label_name = '<i class="fi-comments large"></i>'

    count = user.cached_forum_threads_notifications_count

    if params[:controller] =~ /^forum/ && label_name == 'Forum'
      label_name
    else
      if count > 0
        "#{label_name}<span class='notification-span'><span id='threads-span'>#{greater_than_thousand(count)}</span></span>".html_safe
      else
        "#{label_name}<span class='notification-span'><span id='threads-sup'></span></span>".html_safe
      end
    end

  end

  def forum_thread_status(thread)

    if current_user
      watcher = thread.watchers.where(user_id: current_user.id).limit(1).first
      if watcher.present?
        if watcher.status == 'open' && watcher.read == false
          return 'unread'
        end
      end
    end

    ''
  end

  def short_website(website)
    if website.present?
      uri_domain = URI.parse(website.to_s)

      url = [uri_domain.host, uri_domain.path].join

      if uri_domain.query.present?
        url = [url, uri_domain.query].join('?')
      end

      url
    end
  end

  def get_refresh_collection(refreshable)
    refreshes_array = Market::Refresh.statuses(refreshable.refresh).sort_by{|row| row[0]}#.map{|row| [row[1], row[0]]}
    return_array = {} #[]
    for value, key in refreshes_array
      return_array[key] = value
    end
    # raise return_array
    return_array
  end

  def filter_tab_active(filter, current_tab)
    
    if filter == nil && current_tab == 'freshest'
      return 'active'
    elsif filter != nil && current_tab == filter
      return 'active'
    else
      ''
    end

  end

end
