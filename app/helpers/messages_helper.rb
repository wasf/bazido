module MessagesHelper
  
  def message_read_class(message)

    watcher = Rails.cache.fetch([message, 'watcher',current_user.id]) do
      message.watchers.where(user_id: current_user.id).limit(1).first
    end

    if watcher.read == true
      'read'
    else
      ''
    end

  end
end
