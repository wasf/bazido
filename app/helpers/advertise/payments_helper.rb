module Advertise::PaymentsHelper

  # Checks if the object has been paid for and returns it as a string to be used as a class
  def paid_status(object)
    if object.paid || object.cost == 0.0
      'paid'
    else
      'unpaid'
    end
  end

end
