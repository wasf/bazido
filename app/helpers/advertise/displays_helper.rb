module Advertise::DisplaysHelper
  def display_status(display)
    # TODO: Show the amount of days left for it to show or have be shown
    current_time = Time.now.to_date
    display_time = display.show_at.to_date
    
    if display_time < current_time
      'Past'
    elsif display_time > current_time
      'Next'
    else
      'Current'
    end
  end

end
