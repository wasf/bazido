module UsersHelper
  def short_name(user, username = true)
    if user.profile.username.to_s == '' || !username
      user.profile.full_name.split(' ')[0]
    else
      user.profile.username
    end
  end

  def user_refresh_name(profile)
    # split_name = 
    if profile.username.present?
      name = "@#{profile.username}"
    else

      length_of_name = profile.full_name.length
      name = ''

      if length_of_name < 25
        name = profile.full_name
      else
        full_name_array = profile.full_name.split
        first_and_last_name = "#{full_name_array.first} ... #{full_name_array.last}"

        if first_and_last_name.length < 25
          name = first_and_last_name
        else
          name = "#{profile.full_name[0..22]}"
        end
      end

      name

    end
  end
end
