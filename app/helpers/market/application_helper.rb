module Market
  module ApplicationHelper

    def render_refresh_images(images, size)

      images_array = images.collect do |image|
        img = image_tag(image.display.url(size))
        a_img = link_to img, image.display.url, class: 'th refresh-thumbnails'
        content_tag('li', a_img)
      end

      return content_tag('ul', images_array.join.html_safe, class: 'clearing-thumbs small-block-grid-3 medium-block-grid-4', data: {:clearing => ''})

    end

    def get_refresh_image_id(object)

      input_html = object.file_field(:display)

      regex = /id="(.+)"/
      id_text = regex.match(input_html)[1]
      return id_text+"-image"

    end

    def get_profile_image_id(object)
      input_html = object.file_field(:avatar)

      regex = /id="(.+)"/
      id_text = regex.match(input_html)[1]
      return id_text+"-image"

    end

  end
end
