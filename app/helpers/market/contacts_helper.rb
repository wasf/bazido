module Market
  module ContactsHelper

    def render_contacts(contacts)

      if params[:id].nil? # || !['PUT', 'PATCH'].include?(request.method)

        return render contacts unless contacts.nil?

      else
        # raise contacts
        # The user really wants to edit
        contacts_array = contacts.collect do |contact|
          if contact.id === params[:id].to_i
            render '/market/contacts/form', contact: contact
          else
            render contact
          end
        end

        return contacts_array.join.html_safe

      end

    end

  end
end
