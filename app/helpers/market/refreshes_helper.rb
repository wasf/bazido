module Market
  module RefreshesHelper

    def refresh_status(object)
      Market::Refresh.status_name(Refresh.statuses(object.refresh), object.refresh.status)
    end

    def shorten_refresh_title(title)

      if title.length <= 80
        the_title = title.to_s[0..80]
      else
        the_title = truncate(title.to_s, :length => 80, :separator => ' ')
      end

      return the_title

    end

    def get_refresh_type(refreshable, icon=false)
      
      refresh_type = refreshable.refresh.refreshable_type.split('::').last
      if icon
        case refresh_type
        when 'Accommodation'
          '<i class="fi-home"></i>'
        when 'Book'
          '<i class="fi-book"></i>'
        else
          '<i class="fi-annotate"></i>'
        end
      else
        refresh_type
      end
    end

    def render_refresh_status(refreshable)
      if refreshable.refresh.is_expired?
        'Expired'
      else
        h(Refresh.status_name(Refresh.statuses(refreshable.refresh), refreshable.refresh.status))
      end
    end

    # Returns class to be used to color text/background for a refresh
    # object. Makes it quick to tell it's status just by looking at it
    def render_refresh_status_color(refreshable, text=false)
      if refreshable.refresh.is_available? || refreshable.refresh.is_open?
        status = 'green-status'
      elsif refreshable.refresh.is_expired?
        status = 'orange-status'
      else
        status = 'red-status'
      end

      if !text
        status << '-background'
      end

      status
    end

    def refresh_changes(refreshable)
      changes = []
      if refreshable.refresh.last_allowed == refreshable.refresh.created_at
        changes << 'Bumped: Never'
      else
        changes << 'Bumped: '+refreshable.refresh.last_allowed.strftime('%e %b \'%y at %H:%M')
      end

      changes << "Expire: "+refreshable.refresh.expires.strftime('%e %b \'%y at %H:%M')

      title_changes = changes.join("\n")
      title_changes

    end

  end
end
