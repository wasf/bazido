module DeviseHelper
  def devise_sign_up_error_messages!

    return "" if resource.errors.empty?

    messages = resource.errors.messages.collect do  |key, message|

      full_messages = []
      last_key = key.to_s.split(".").last

      for msg in message

        full_message = "#{last_key.humanize} #{msg}"
        full_messages << content_tag(:li, full_message)

      end

      full_messages.join

    end

    sentence = I18n.t('errors.messages.not_saved', :count => resource.errors.count, :resource =>resource.class.model_name.human.downcase)

    html = <<-HTML
    <div id="error_explanation">
      <h3>#{sentence}</h3>
      <ul>#{messages.join}</ul>
    <div>
    HTML

    html.html_safe
  end

  def devise_error_messages!
    return "" if resource.errors.empty?

    # raise resource
    messages = resource.errors.full_messages.map {|msg| content_tag(:li, msg) }.join
    sentence= I18n.t('errors.messages.not_saved', :count => resource.errors.count, :resource =>resource.class.model_name.human.downcase)

    html = <<-HTML
    <div id="error_explanation">
      <h3>#{sentence}</h3>
      <ul>#{messages}</ul>
    <div>
    HTML

    html.html_safe
  end

  def devise_error_messages?
    resource.errors.empty? ? false : true
  end
end
