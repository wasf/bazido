module NokogiriFilter
  def remove_tags_and_preserve_content(*list)

    xpath("//*").each do |element|
      if list.include?(element.name)
        element.children.reverse.each do |child|
          child_clone = child.clone
          element.add_next_sibling child_clone
          child.unlink
        end
        element.unlink
      end
    end

  end
end

class Nokogiri::XML::Element
  include NokogiriFilter
end

class Nokogiri::XML::NodeSet
  include NokogiriFilter
end
