class Profile < ActiveRecord::Base
  include IdentityCache

  belongs_to :user, touch: true

  has_many :academics, :dependent => :destroy
  has_many :employments, :dependent => :destroy

  avatar_options = {default_url: '/displays/small/missing.png', :styles => {:small => ['80x64>', :jpg], :medium => ['320x256>', :jpg]}, :convert_options => { :display_picture => '-quality 95 -strip', :icon => '-strip'}}

  unless Rails.env.development?
    # raise avatar_options
    avatar_options.merge! :storage        => :s3,
      :s3_credentials => "#{Rails.root}/config/s3.yml",
      :path           => ':attachment/:id/:style.:extension',
      :bucket         => 'bazido-user'
  end

  has_attached_file :avatar, avatar_options

  cache_has_many :academics, embed: true
  cache_has_many :employments, embed: true

  @@genders = %w(male female)

  before_validation :prepare_data
  before_validation :make_institution_website, :if => lambda {|c| c.original_institution_website.present? }
  before_post_process :check_file_size

  validates_attachment_content_type :avatar, :content_type => /^image\/(png|gif|jpeg)/
  validates_attachment_size :avatar, :less_than => 1.megabytes
  validates_presence_of :full_name
  validates_length_of :full_name, :within => 3..35
  validates_uniqueness_of :username, :if => lambda {|c| c.username.present? }
  validates_length_of :username, :within => 6..20, :if => lambda {|c| c.role.present? && c.username.present? && c.role != 'admin' }
  validates_presence_of :original_institution_website
  validates_inclusion_of :gender, :in => @@genders, :if => lambda {|c| c.gender.present? }

  accepts_nested_attributes_for :academics, :update_only => true, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :employments, :update_only => true, :allow_destroy => true, :reject_if => :all_blank

  def self.genders
    @@genders.collect do |value|
      [value.capitalize, value]
    end
  end

  def to_mongo
    json_object = JSON.parse(to_json(:only => [:gender, :id, :institution_website, :location, :role]))
    json_object['academics'] = self.academics.collect do |academic|
      academic.to_mongo
    end
    json_object
  end

  private
    
    def check_file_size
      valid?
      errors[:display_file_size].blank?
    end

    def prepare_data
      self.full_name = self.full_name.to_s.split(" ").join(" ")
      self.username = clean_up_username(self.username.to_s) unless self.username.nil?# First username clean
      self.username = self.username.downcase.parameterize unless self.username.nil?
      self.username = clean_up_username(self.username) unless self.username.nil? # Removes whatever characters "parameterize found illegal
      self.website = clean_up_url(self.website)
      self.gender = (self.gender.to_s.downcase.strip)[0..10]

    end

    def clean_up_username(username)
      # Remove spaces
      username = username.gsub(/(\s+)/, '')

      # Remove consecutive underscore
      username = username.gsub(/(_+)/, '_')
      
      # Remove consecutive dashes
      username = username.gsub(/(-+)/,'_')

      username
    end

    def make_institution_website

      # Check if the domain has http attached to it
      if self.original_institution_website != nil
       
        # All that matters is that it uses the http protocol
        self.original_institution_website = "http://#{self.original_institution_website.strip}" unless (/\Ahttps?:\/\/(.)*\z/i =~ self.original_institution_website) != nil
        # self.original_institution_website = "http://#{self.original_institution_website.strip}" unless self.original_institution_website.to_s.downcase[0...4] == 'http'

      end

      # puts self.original_institution_website

      begin
        uri_parse = URI.parse(self.original_institution_website)
        uri_host = uri_parse.host
      rescue
        uri_host = nil
      end

      if uri_host != nil
        host = uri_host
      else
        host = self.original_institution_website
      end

      # Extract the 
      begin

        domain = PublicSuffix.parse(host)

        if domain.domain != nil
          self.institution_website = domain.domain.downcase
        end

      rescue PublicSuffix::DomainInvalid, PublicSuffix::DomainNotAllowed

        self.errors.add(:orignal_institution_website, I18n.translate('model.profile.make_institution_website.invalid_original_website_institution'));
        # false
      end

    end

    def clean_up_url(url)

      url = url.to_s
      if url != ""

        # url = "http://#{url.strip}" unless url.downcase[0...4] == 'http'
        url = "http://#{url.strip}" unless (/\Ahttps?:\/\/(.)+/i =~ url) != nil
        
        begin
          domain = URI.parse(url)
          return domain.to_s
        rescue URI::InvalidURIError
          nil
        end
      end

    end

end
