class Message < ActiveRecord::Base
  include IdentityCache

  belongs_to :messageable, polymorphic: true, touch: true
  belongs_to :user

  has_many :responses, dependent: :destroy
  has_many :watchers, as: :watchable, class_name: 'Watch'

  cache_has_many :responses, embed: true
  cache_has_many :watchers, inverse_name: :watchable, embed: true

  validates_presence_of :title

  before_validation :prepare_data
  after_commit :flush_cache

  accepts_nested_attributes_for :responses

  def flush_cache
    keys = self.watchers.where(status: 'open').pluck(:user_id).collect do |k|
      ['messages', k]
    end
    Rails.cache.delete(keys)
  end

  def self.cached_user_messages(user, page_num, message_filter, statuses)
    message = Rails.cache.fetch(['messages', user]) do
      block_messages = self.includes(:responses).joins(:watchers).select('DISTINCT(messages.id), messages.*').where('watches.user_id=? AND watches.status IN (?)', user.id, statuses).references(:watches).order(updated_at: :DESC)
      if message_filter.present?
        block_messages = block_messages.where(application_space: message_filter.strip)
      end

      block_messages

    end

    message

  end

  protected

    def prepare_data
      self.title = self.title.split.join(' ')
    end
end
