class Academic < ActiveRecord::Base
  include IdentityCache

  belongs_to :profile, touch: true

  before_validation :prepare_data

  validates_presence_of :institution
  validates_presence_of :department
  validates_presence_of :code
  validates_presence_of :name
  validates_presence_of :commence
  validates_presence_of :graduation
  
  validates_numericality_of :graduation, :only_integer => true, :greater_than_or_equal_to => :commence, :if => lambda {|c| c.commence.present? }

  def to_mongo
    JSON.parse(to_json(only: [:id, :college_name, :department, :code, :name, :commence, :graduation]))
  end

  private

    def prepare_data
      self.institution = self.institution.strip
      self.department = self.department.to_s.strip
      self.code = self.code.to_s.strip
      self.name = self.name.to_s.strip
      self.commence = self.commence.to_i
      self.graduation = self.graduation.to_i
    end
end
