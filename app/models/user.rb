class User < ActiveRecord::Base
  include IdentityCache

  attr_accessor :terms_and_conditions

  has_one :profile, :dependent => :destroy

  has_many :messages, as: :messageable
  has_many :refreshes, class_name: 'Market::Refresh'
  # has_many :accommodations, class_name: 'Market::Accommodation'
  # has_many :books, class_name: 'Market::Refresh', source_type: 'Market::Book'

  has_many :advertisements, class_name: 'Advertise::Advertisement'
  has_many :sponsors, class_name: 'Advertise::Sponsor'
  has_many :payments, class_name: 'Advertise::Payment'

  cache_has_one :profile, embed: true
  cache_has_many :messages
  cache_has_many :refreshes, embed: true
  cache_has_many :advertisements
  cache_has_many :sponsors
  cache_has_many :payments

  # Include default devise modules. Others available are:
  # :timeoutable, :omniauthable
  devise :database_authenticatable, :registerable, :lockable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_acceptance_of :terms_and_conditions, :if => :check_terms_and_conditions?

  accepts_nested_attributes_for :profile, :update_only => true

  def owner_id
    self.id
  end

  def to_mongo
    json_hash = JSON.parse(to_json(only: [:email, :id]))
    json_hash['profile'] = self.profile.to_mongo
    json_hash
  end

  def balance(to_float = true)

    paid_to_us = Advertise::Advertisement.paid(self)
    paid_to_us += Advertise::Sponsor.paid(self)

    has = Advertise::Payment.for_user(self) - paid_to_us

    if to_float
      has.to_f
    else
      has
    end
  end

  def owes(to_float = true)

    owed_to_us = Advertise::Advertisement.owes(self)
    owed_to_us += Advertise::Sponsor.owes(self)

    if to_float
      owed_to_us.to_f
    else
      owed_to_us
    end

  end

  def cached_users_count(institution_website=nil)
    users_count = Rails.cache.fetch(['users', institution_website]) do

      object = User.where(activated: true)
      
      if institution_website
        object = object.where('profiles.institution_website = ?', institution_website).count
      end
      
      object.count
    end

    users_count

  end

  # Shows how much the user would be left if the paid for all the services they want us to fulfill. Eg. Advertisements and Sponsor
  def credits(to_float = true)
    
    owes = Advertise::Advertisement.owes(self)
    owes += Advertise::Sponsor.owes(self)

    
    has = balance

    amount = has - owes
    
    amount.to_f

  end

  def is_admin?
    self.profile.role == 'admin'
  end

  def message_notifications
    watcher_notifications(Message)
  end

  def forum_threads_notifications
    watcher_notifications(Forum::Thread)
  end

  def watching_forum_threads(k=Forum::Thread)
    # the 'where' overrides the one that already exists
    watcher_notifications(k, nil)
  end

  def cached_profile_role
    Rails.cache.fetch([self, 'role']) do
      self.profile.role
    end
  end

  def cached_message_notifications_count
    Rails.cache.fetch([self, 'message_notifications_count']) do
      message_notifications.count
    end
  end

  def cached_forum_threads_notifications_count
    Rails.cache.fetch([self, 'forum_threads_notifications_count']) do
      forum_threads_notifications.count
    end
  end

  def courses
    Learn::Attendee.where(user_id: self.id)
  end
  
  # TODO: Read in to and out of cache
  def cached_course_count
    courses.count
  end

  def cached_courses
    courses
  end

  def all_course_questions(page_number=nil, page_size=30)
    Learn::Question.where(course_id: courses.pluck(:course_id)).page(page_number.to_i).per(page_size.to_i)
  end

  protected

    def check_terms_and_conditions?
      if self.id == nil
        true
      else
        false
      end

    end

    def watcher_notifications(klass, read=false)

      object = klass.joins(:watchers).where('watches.user_id=?', self.id).where('watches.status=?', 'open').references('watches')
      if !read.nil?
        object = object.where('watches.read=?', read)
      end

      object

    end

    def confirmation_required?
      false
    end
end
