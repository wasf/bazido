module Forum
  class Comment < ActiveRecord::Base

    include ::IdentityCache

    STATUSES = %w(visible deleted)

    belongs_to :user
    belongs_to :thread, touch: true

    before_validation :prepare_data

    validates_presence_of :content
    validates_presence_of :reason, if: lambda {|c| c.status.present? && c.status != 'visible'}

    def cached_thread
      Rails.cache.fetch([self.thread]) do
        self.thread
      end
    end

    def self.statuses(for_select = false)
      
      statuses = STATUSES.sort

      if for_select
        statuses = statuses.collect do |status|
          [status.humanize, status]
        end
      end

      statuses
    end

    protected

      def prepare_data
        self.content = self.content.strip
        self.status = 'visibile' if self.status.nil?
      end

  end
end
