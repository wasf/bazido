module Forum
  class Thread < ActiveRecord::Base
    include ::IdentityCache

    CATEGORIES = %w(campus residence news events general computers bazido study_center)
    STATUSES = %w(closed banned open)
    
    belongs_to :user
    has_many :comments, dependent: :destroy, class_name: 'Comment'
    has_many :watchers, as: :watchable, class_name: 'Watch'
    # cache_has_many :comments, inverse_name: :comment, embed: true
    # cache_has_many :watchers, inverse_name: :watchable

    before_validation :prepare_data
    after_commit :flush_cache

    validates_presence_of :title
    validates_presence_of :content
    validates_presence_of :category
    validates_presence_of :reason, if: lambda {|c| c.status.present? && c.status != 'open'}

    validates_length_of :title, within: 10..255
    validates_length_of :content, minimum: 10

    validates_inclusion_of :category, in: CATEGORIES
    validates_inclusion_of :status, in: STATUSES

    # include ::Elasticsearch::Model
    # include ::Elasticsearch::Model::Callbacks

    # settings index: { number_of_shards: ::Market::NUMBER_OF_ELASTICSEARCH_SHARDS } do

    #   mappings dynamic: 'false' do
    #     indexes :title, analyzer: 'english'
    #     indexes :content, analyzer: 'english'
    #   end
    # end

    scope :for_user, lambda {|user|
      joins(:comments).where('forum_comments.thread_id=forum_threads.id AND forum_comments.user_id=?', user.id).references(:forum_comments).select('DISTINCT(forum_threads.id), forum_threads.*')
    }

    def flush_cache
      keys = CATEGORIES.collect do |category|
        [self.institution_website, category]
      end

      Rails.cache.delete(keys)
    end

    def self.cached_for_institution(institution_website, category, page)
      threads = Rails.cache.fetch([institution_website, category]) do
        threads = Forum::Thread.where('institution_website=?', institution_website).includes(:comments, :user)

        if category.present?
          threads = threads.where(category: category)
        end

        threads = threads.order(updated_at: :DESC)
        threads
      end

      threads.page(page)
    end

    def self.categories(for_select = false)

      categories = CATEGORIES.sort
      if for_select
        categories = categories.collect do |category|
          [category.humanize, category]
        end
      end

      categories
    end

    def self.statuses(for_select = false)
      
      statuses = STATUSES.sort

      if for_select
        statuses = statuses.collect do |status|
          [status.humanize, status]
        end
      end

      statuses
    end

    def cached_visible_comment_count
      Rails.cache.fetch([self,'comment_count']) do
        self.comments.where(status: 'visible').count
      end
    end

    protected

      def prepare_data
        self.title = self.title.split.join(' ')
        self.content = self.content.strip
        self.category = self.category.strip.downcase
        self.status = 'open' if self.status.nil?
      end

  end
end
