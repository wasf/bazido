module Learn
  class Question < ActiveRecord::Base
    CATEGORIES = %w(question discussion)
    STATUSES = %w(open closed)
    belongs_to :course
    belongs_to :user

    has_many :answers, dependent: :destroy
    has_many :watchers, as: :watchable, class_name: 'Watch'

    validates_presence_of :title
    validates_length_of :title, minimum: 5
    validates_presence_of :content
    validates_length_of :content, minimum: 5

    before_validation :prepare_data

    def self.categories(for_form = false)
      if for_form
        CATEGORIES.collect do |category|
          [category.humanize, category]
        end
      else
        CATEGORIES
      end
    end

    def self.statuses(for_form)
      if for_form
        STATUSES.collect do |category|
          [category.humanize, category]
        end
      else
        STATUSES
      end
    end

    protected
      def prepare_data
        self.title = self.title.to_s.split.join(' ')
        self.content = self.content.to_s.strip
        self.status = 'open' if self.status.nil?
      end
    
  end
end
