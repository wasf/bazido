class Learn::Answer < ActiveRecord::Base

  STATUSES = %w(open closed)

  belongs_to :question
  belongs_to :user

  before_validation :prepare_data

  validates_presence_of :content
  validates_length_of :content, minimum: 5

  def self.statuses(for_form)
    if for_form
      STATUSES.collect do |category|
        [category.humanize, category]
      end
    else
      STATUSES
    end
  end

  protected
    def prepare_data
      self.content = self.content.to_s.strip
      self.status = 'open' if self.status.nil?
    end
end
