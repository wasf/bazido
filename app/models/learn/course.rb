module Learn
  class Course < ActiveRecord::Base
    
    has_many :questions, dependent: :destroy
    has_many :discussions, dependent: :destroy
    has_many :attendees, dependent: :destroy

    validates_presence_of :code
    validates_presence_of :name

    validates_format_of :code, with: /\A([a-z]){2,5}\s*([0-9]){2,5}([a-z]){0,2}\z/i
    validates_uniqueness_of :internal_code, scope: :institution_website

    before_validation :prepare_data
    after_validation :move_internal_code_errors

    def to_param
      self.internal_code
    end

    def coursarize(code)
      code.to_s.gsub(/[^0-9a-z]/i, '').downcase
    end

    protected

      def prepare_data
        self.code = self.code.split.join(' ')
        self.internal_code = coursarize(self.code)
        self.name = self.name.split(' ').join(' ')
      end

      # Will make self.code display the internal_code errors
      def move_internal_code_errors
        self.errors[:internal_code].to_a.each do |error|
          self.errors.add(:code, error)
        end
      end
  end
end
