class Advertise::Payment < AdvertiseClass
  include ::IdentityCache
  belongs_to :user, touch:true

  before_validation :prepare_data
  serialize :params

  def self.for_user(user)
    # raise user
    self.where(user_id: user.id).where('advertise_payments.date_paid IS NOT NULL').where(refunded: false).sum(:amount)
  end

  protected

    def prepare_data
      self.refunded = false if self.refunded == nil
    end

end
