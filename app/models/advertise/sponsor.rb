module Advertise
  class Sponsor < AdvertiseClass
    include ::IdentityCache
    include Sponsorable

    belongs_to :user, touch: true

    has_many :displays, as: :displayable, dependent: :destroy
    cache_has_many :displays, inverse_name: :displayable

    validates_presence_of :title
    validates_presence_of :link

    before_validation :prepare_data

    accepts_nested_attributes_for :displays, update_only: true, reject_if: lambda {|current_hash| Display.invalid_display?(current_hash) }, allow_destroy: true

  paperclip_options_display = {:dependent => :destroy, default_url: '/displays/small/missing.png',  :styles => {:small => ['768x192#', :jpg]}, :convert_options => {:small => '-strip'}}

  unless Rails.env.development?
    paperclip_options_display.merge! :storage        => :s3,
      :s3_credentials => "#{Rails.root}/config/s3.yml",
      :path           => ':attachment/:id/:style.:extension',
      :bucket         => 'bazido-serve'
  end

    has_attached_file :display, paperclip_options_display

    validates_attachment_content_type :display, :content_type => /^image\/(png|gif|jpeg)/
    validates_attachment_size :display, :less_than => 64.kilobytes

    # Has to be below the attachment stuff
    before_post_process :check_file_size

    def to_mongo
      json_hash = JSON.parse(to_json(only: [:user_id, :banned, :link, :paid]))
      json_hash[:display] = self.display.url(:small)
      json_hash[:displays] = self.displays.collect do |display|
        display.to_mongo
      end

      json_hash
    end

    protected
      def prepare_data

      end

      def check_file_size
        valid?
        errors[:display_file_size].blank?
      end


  end
end
