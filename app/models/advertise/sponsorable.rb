module Advertise
  module Sponsorable

    extend ActiveSupport::Concern

    USER_CHANGE_RATE = 180

    module ClassMethods

      def all_costs(user, paid = nil)
        sponsorable_name = self.table_name

        object = joins(:displays).where("advertise_displays.displayable_id=#{sponsorable_name}.id AND advertise_displays.displayable_type=?", name).where('user_id=?', user.id).where('advertise_displays.free=?', false).references(:advertise_displays)
        # object = Sponsor.includes(:displays).where('user_id=?', user.id)
        if !paid.nil?
          object = object.where('paid=?', paid)
        end
        
        # Calculate the costs
        object = object.sum('advertise_displays.cost * advertise_displays.consecutive_days')
        # ===================
        
        object
      end

      def paid(user)
        all_costs(user, true)
      end

      def owes(user)
        all_costs(user, false)
      end

      def invested(user)
        all_costs(user, nil)
      end

      # user is the user that must see this advert
      # If there is no user, the object's institution will be used to pick an ad
      def advertise(user, object)
        
        # This caches rows that have the adverts that need to be displayed
          case self.name
          when 'Advertise::Advertisement'
            expire_time = 30.minutes
          when 'Advertise::Sponsor'
            expire_time = 1.hour
          else
            expire_time = 40.minutes
          end

        institution_data = get_instutition_website(user, object)
        if !institution_data.nil?

          # Determine wether to cal sponsors or advertisements
          method_name = self.name.split('::').last.downcase << 's'
          advertisements = Rails.cache.fetch("#{institution_data[0]}-#{method_name}", expires_in: expire_time) do
            # Call the method
            send(method_name, institution_data[0])
          end


          advertisements_length = advertisements.try(:length)

          if !advertisements.nil? && advertisements.any?
            # There are ads, now determine if the user has seen one
            if user.present?
              
              # Super counter is always changing, this is how users see random adverts
              super_counter_name = "#{institution_data[0]}-super-counter"
              # super_counter = Rails.cache.fetch(super_counter_name, raw: true) do
              #   rand(advertiments.length)
              # end

              # I save the user_data as json because for some reason,
              # when passing it to the reset_if_time_up method, it suddenly becomes a string
              # that can't be changed to a hash again
              user_data_json = Rails.cache.fetch(institution_data[1], expires_in: USER_CHANGE_RATE, raw: true) do

                spc = Rails.cache.increment(super_counter_name, 1)
                if spc >= advertisements_length
                  Rails.cache.write(super_counter_name, 0, raw: true, expires_in: expire_time)
                end

                hash = {user_id: user.id, index: spc, created_at: Time.now, key: institution_data[1]}
                hash.to_json

              end

              user_data = JSON.parse(user_data_json)

              # Reset the user_data
              reset_if_time_up(user_data, advertisements_length)
              # raise user_data
              # Use the user data to
              return advertisements[user_data['index'] % advertisements_length]

            else

              institution_data_name = institution_data.join('_')
              # Show something random
              institution_counter = Rails.cache.fetch(institution_data_name, raw: true, expires_in: expire_time) do
                rand(advertisements_length)
              end
              
              Rails.cache.increment(institution_data_name, 1)

              institution_counter_string = Rails.cache.read(institution_data_name)
              institution_counter = institution_counter_string.value

              if institution_counter > advertisements_length
                Rails.cache.write(institution_data_name, 0, raw: true, expires_in: expire_time)
              end

              # This will returns a new advert for non-logged in users
              return advertisements[institution_counter % advertisements_length]
            end
          else
            nil
          end

        else
          nil
        end
      end

      # user is the user that must see this advert
      # If there is no user, the object's institution will be used to pick an ad
      def sponsor(user, object)
      end

      # Resets a user's data from RAM if the time has expired so they can see a different advert
      def reset_if_time_up(user_data, advertisement_length)
        # The change rate will be in seconds
        change_rate = USER_CHANGE_RATE

        Rails.cache.delete(user_data['key']) if user_data['created_at'] < change_rate.seconds.ago

        change_rate
      end

      def advertisements(institution_website)
        
        object = advertisable_selection(institution_website).where('show_at < ?', Time.now).where('show_til > ?', 30.minutes.from_now)
        object
      end

      def sponsors(institution_website)
        object = advertisable_selection(institution_website).where('show_at::date < ?', Date.today).where('show_til > ?', Time.now.to_date)
        object
      end

      def advertisable_selection(show_til, institution_website = nil)
        object = joins(:displays).where('paid=? OR advertise_displays.free=?', true, true).references(:advertise_displays)

        if institution_website != nil && institution_website != false
          object = object.where('advertise_displays.institution_website = ?', institution_website)
        end

        object

      end

      private

        def get_instutition_website(user, object)

          if !user.nil?
            [user.profile.institution_website, "#{User.name}-#{user.id}"]
          elsif !object.nil? # && object.try(:institution_website)

            # Check the possible class that might have a instution_website
            first_part_object_class_name = object.class.name.split('::').first
            case first_part_object_class_name
            when 'Market'
              institution_website = object.refresh.institution_website
            else
              institution_website = nil
            end

            if institution_website
              [institution_website, "#{object.class.name}-#{object.id}"]
            else
              # Don't have a institution_website to target the user with
              nil
            end

          else
            nil
          end
        end

    end

    def cost
      self.displays.where('free <> ?', true).sum('cost * consecutive_days').to_f
    end

  end
end
