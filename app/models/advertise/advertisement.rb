module Advertise
  class Advertisement < AdvertiseClass
    include ::IdentityCache
    include Sponsorable

    belongs_to :user, touch: true

    has_many :displays, as: :displayable, dependent: :destroy

    cache_has_many :displays, inverse_name: :displayable, embed: true

    validates_presence_of :title
    validates_presence_of :link

    before_validation :prepare_data
    after_commit :flush_cache

    accepts_nested_attributes_for :displays, update_only: true, reject_if: lambda {|current_hash| Display.invalid_display?(current_hash) }, allow_destroy: true

  # paperclip_options_display = {default_url: '/displays/small/missing.png', :dependent => :destroy,  :styles => {:small => '256x256>'}, :convert_options => {:small => '-strip'} }
  paperclip_options_display = {default_url: '/displays/small/missing.png', :dependent => :destroy,  :styles => {:small => '320x189>'}, :convert_options => {:small => '-strip'} }

  unless Rails.env.development?
    paperclip_options_display.merge! :storage        => :s3,
      :s3_credentials => "#{Rails.root}/config/s3.yml",
      :path           => ':attachment/:id/:style.:extension',
      :bucket         => 'bazido-serve'
  end

    has_attached_file :display, paperclip_options_display

    # paperclip_options_display_mobile = {default_url: '/displays/small/missing.png', :dependent => :destroy,  :styles => {:small => ['768x160#', :jpg]}, :convert_options => {:small => '-strip'}}

  # unless Rails.env.development?
    # paperclip_options_display_mobile.merge! :storage        => :s3,
    #   :s3_credentials => "#{Rails.root}/config/s3.yml",
    #   :path           => ':attachment/:id/:style.:extension',
    #   :bucket         => 'bazido-serve'
  # end

    # has_attached_file :display_mobile, paperclip_options_display_mobile

    validates_attachment_content_type :display, :content_type => /^image\/(png|gif|jpeg)/
    # validates_attachment_content_type :display_mobile, :content_type => /^image\/(png|gif|jpeg)/
    validates_attachment_size :display, :less_than => 64.kilobytes
    # validates_attachment_size :display_mobile, :less_than => 64.kilobytes

    # Has to be below the attachment stuff
    before_post_process :check_file_size

    def flush_cache
      Rails.cache.delete(['advertisements', self.user])
    end

    def to_mongo
      json_hash = JSON.parse(to_json(only: [:id, :user_id, :banned, :link, :paid]))
      json_hash[:display] = self.display.url(:small)
      # json_hash[:display_mobile] = self.display_mobile.url(:small)
      json_hash[:displays_attributes] = self.displays.collect do |display|
        display.to_mongo
      end

      json_hash
    end

    def self.cached_for_user(user)

      advertisements = Rails.cache.fetch(['advertisements', user]) do
        self.includes(:displays).where(user_id: user.id).order(created_at: :DESC).all
      end

      advertisements
    end

    protected
      def prepare_data

      end

      def check_file_size
        valid?
        errors[:display_file_size].blank?
      end

  end
end
