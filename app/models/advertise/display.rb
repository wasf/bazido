module Advertise
  class Display < AdvertiseClass
    include ::IdentityCache

    belongs_to :displayable, polymorphic: true, touch: true

    validates_numericality_of :cost, greater_than_or_equal: 0.0
    validates_numericality_of :consecutive_days, within: 1..60

    before_validation :prepare_data

    def self.invalid_display?(hash)
      hash_time = Time.mktime(hash['show_at(1i)'], hash['show_at(2i)'], hash['show_at(3i)'], hash['show_at(4i)'], hash['show_at(5i)']) 
      earliest_time_allowed = 1.hour.from_now.utc
      latest_time_allowed = 6.weeks.from_now.utc
     
      # if hash['free'] is nil, user is not admin
      if (hash_time < earliest_time_allowed || hash_time > latest_time_allowed) && !hash['free'].present?
        # This display is invalid
        return true
      end

      false

    end

    def self.advertisement_prices
      [8.65, 8.05, 7.65, 6.80]
    end

    def self.sponsor_prices
      [23.5, 21.6, 19.55, 17.5]
    end

    def to_mongo
      json_hash = JSON.parse(to_json(only: [:id, :institution_website, :consecutive_days, :displayable_type, :displayable_id, :free, :cost, :locked]))
      # json_hash['_id'] = self.id
      json_hash['show_start']  = self.show_at
      json_hash['show_end'] = (self.show_at + self.consecutive_days.days)

      json_hash
    end

    protected

      def prepare_data
        self.consecutive_days = self.consecutive_days.abs
        self.cost = self.cost.abs

        self.show_til = self.show_at + self.consecutive_days.days
        self.institution_website = short_url(self.institution_website)
      end

      def short_url(url)
        begin

          domain = PublicSuffix.parse(url.to_s)

          if domain.domain != nil
            self.institution_website = domain.domain.downcase
            return domain.domain
          end

        rescue PublicSuffix::DomainInvalid, PublicSuffix::DomainNotAllowed
          self.errors.add(:base, 'Invalid institution_website')
        end

        nil

      end
  end
end
