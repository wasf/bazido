module Mongo
  class Academic
    include Mongoid::Document

    field :college_name, type: String
    field :department, type: String
    field :code, type: String
    field :name, type: String
    field :commence, type: Integer
    field :graduation, type: Integer
  end
end
