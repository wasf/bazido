module Mongo
  class Profile
    include Mongoid::Document
    
    field :gender, type: String
    field :institution_website, type: String
    field :location, type: String
    field :role, type: String
    
    embeds_many :academics, class_name: 'Mongo::Academic'
  end
end
