module Mongo
  class User
    include Mongoid::Document
    field :email, type: String
    embeds_one :profile, class_name: 'Mongo::Profile'
  end
end
