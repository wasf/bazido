# TODO: Validate the given date range for how long a user has worked a a company. Currently errors don't appear correctly
class Employment < ActiveRecord::Base
  include IdentityCache

  belongs_to :profile, touch: true

  before_validation :prepare_data
  before_validation :valid_employment_range, :if => lambda {|c| !c.current_employer && c.start_date.present? && c.end_date.present? }

  validates_presence_of :company
  validates_presence_of :position
  validates_presence_of :location

  private

    def prepare_data
      self.company = self.company.to_s.split(" ").join(" ")
      self.position = self.position.to_s.split(" ").join(" ")
      self.location = self.location.to_s.split(" ").join(" ")
      self.description = self.description.to_s.strip
      # raise self
    end

    def valid_employment_range
      start_date = self.start_date.to_i
      end_date = self.end_date.to_i

      if end_date < start_date
        # Change the date so that it is like the start_date
        self.end_date = self.start_date
      end

      # raise self

    end

end
