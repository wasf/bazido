# This is a way to make payments through paypal
class Pay
  
  def self.paypal_url(user, amount, return_url, notify_url) 
    if amount < 6
      amount = 6
    end

    values = { 
      :business => 'HXJ3RR9G3SR52',
      :cmd => '_cart',
      :upload => 1,
      :return => return_url,
      :currency_code => 'USD',
      :custom => "#{user.id}",
      :notify_url => notify_url
    }   
    values.merge!({ 
      "amount_1" => amount.abs, # unit_price,
      "item_name_1" => 'BaziDo Advertising', # The plus is for URL purposes
      "item_number_1" => "#{user.id}-#{Time.now.strftime('%Y%m%d')}-#{SecureRandom.hex(8)}",
      "quantity_1" => '1'
    })

    "https://www.paypal.com/cgi-bin/webscr?" + values.to_query
  end 

end

