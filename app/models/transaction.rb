class Transaction < ActiveRecord::Base
  attr_reader :transaction_id, :date_paid
  validates_presence_of :amount
  validates_numericality_of :amount, greater_than_or_equal_to: 5.0

  # Returns true if the payment was made else false
  def pay(remote_ip)

    amount_being_paid = convert_amount(self.amount)
    credit_card = extract_credit_card_details

    if credit_card.validate.empty?
      # Capture the amount being paid
      response = GATEWAY.purchase(amount_being_paid, credit_card, ip: remote_ip)

      if response.success?
        # Return true and leave the document intact
        @transaction_id = response.params['TransactionID']
        @date_paid = response.params['timestamp']
        return true
      else
        self.errors.add(:base, response.message)
        return false
      end
    else
      # The credit card is invalid
      credit_card.valid?
      copy_credit_card_errors(credit_card)
      self.errors.add(:base, 'Credit card information is invalid')
      false
    end

  end

  private

    def convert_amount(amount)
      amount *= 100 # Because we have it in dollars, convert to cents
      amount.to_i
    end

    def extract_credit_card_details

      credit_card = ActiveMerchant::Billing::CreditCard.new(
        :first_name => self.first_name,
        :last_name => self.last_name,
        :number => clean_number(self.card_number),
        :month => self.month,
        :year => self.year,
        :verification_value => self.verification_value,
        :brand => self.card_type
      )

      credit_card

    end

    # Allows users to have spaces in the numbers, this function
    # strips them out just for the purpose of paying
    def clean_number(number)
      card_number = ''
      number.to_s.split('').each do |c|
        if is_number?(c)
          card_number << c
        end
      end
      card_number
    end

    def is_number?(char)
      true if Float(char) rescue false
    end

    # Copies errors in the credit card object to itself
    def copy_credit_card_errors(credit_card)

      # The fields from model that have been swapped
      swapped_fields = {
        'number' => 'card_number',
        'brand' => 'card_type'
      }
      credit_card.errors.each do |key, value_array|
        if value_array.kind_of?(Array)
          # Add every error in the value array
            key = swapped_fields[key] if swapped_fields[key].present?
          value_array.each do |value|
            self.errors.add(key.to_sym, value)
          end
        else
          self.errors.add(key.to_sym, value_array)
        end
      end
    end

end
