class Ability
  include CanCan::Ability

  def initialize(user)

    # This everyone can do

      # This user is a guest user
      can [:read, :market], User

      # Accommodation
      can :show, Market::Accommodation do |accommodation|
        accommodation.refresh.is_public == true
      end

      # Books
      can :show, Market::Book do |book|
        book.refresh.is_public == true
      end

    if !user.nil?

      # This is a logged in user
      if user.cached_profile_role == 'user'

        # User
        can [:read, :market], User
        can :update, User do |user_object|
          user.id == user_object.id
        end

        # Profile
        can :read, Profile
        can :update, Profile do |profile|

          user.id == profile.user_id

        end

        # Market::Accommodation
        can [:show, :index, :search], Market::Accommodation do |accommodation|
          accommodation.refresh.is_public == true || accommodation.refresh.user_id == user.id
      end
        can [:new, :create], Market::Accommodation do |accommodation|
          user_confirmed?(user)
          # true
        end
        can [:edit, :update, :destroy], Market::Accommodation do |accommodation|
          user.id == accommodation.refresh.user_id
        end

        # Market::Book
        can [:show, :index, :search], Market::Book do |book|
          book.refresh.is_public == true || book.refresh.user_id == user.id
        end
        can [:new, :create], Market::Book do |book|
          user_confirmed?(user)
        end
        can [:edit, :update, :destroy], Market::Book do |book|
          user.id == book.refresh.user_id
        end

        # Market::Contact
        can :create, Market::Contact

        can [:show, :index, :destroy, :update], Market::Contact do |contact|
          # Users can only see their own contacts
          contact.user_id == user.id
        end

        # Market::Refresh
        can [:update], Market::Refresh do |refresh|
          refresh.user_id == user.id
        end
        can [:refresh], Market::Refresh do |refresh|
          refresh.user_id == user.id && refresh.next_allowed < Time.now
        end


        # Advertise
        can [:new, :create], Advertise::Advertisement do |advertisement|
          user_confirmed?(user)
        end
        can [:show, :index], Advertise::Advertisement do |advertisement|
          user.id == advertisement.user_id
        end
        can [:update], Advertise::Advertisement do |advertisement|
          # Must be mine and the earliest display date must still be coming
          user.id == advertisement.user_id &&
            advertisement.paid == false
        end
        can [:destroy], Advertise::Advertisement do |advertisement|
          user.id == advertisement.user_id &&
            advertisement.paid == false
        end

        can [:activate], Advertise::Advertisement do |advertisable|

          advertisable.paid == false &&
            advertisable.cost > 0.0 &&
            (user.balance - advertisable.cost) >= 0.0 &&
            advertisable.banned == false &&
            ( advertisable.displays.order(show_at: :ASC).none? ||
            advertisable.displays.order(show_at: :ASC).first.show_at.to_date > 1.hour.from_now)

        end
        # ===================================

        # Advertise
        can [:new, :create], Advertise::Sponsor do |sponsor|
          user_confirmed?(user)
        end

        can [:show, :index], Advertise::Sponsor do |sponsor|
          user.id == sponsor.user_id
        end

        can [:update], Advertise::Sponsor do |sponsor|
          # Must be mine and the earliest display date must still be coming
          user.id == sponsor.user_id &&
            sponsor.paid == false
        end

        can [:destroy], Advertise::Sponsor do |sponsor|
          user.id == sponsor.user_id &&
            sponsor.paid == false
        end

        can [:activate], Advertise::Sponsor do |advertisable|

          advertisable.paid == false &&
            (user.balance - advertisable.cost) >= 0.0 &&
            advertisable.banned == false &&
            ( advertisable.displays.order(show_at: :ASC).none? ||
            advertisable.displays.order(show_at: :ASC).first.show_at.to_date > 1.day.from_now)

        end

        can [:index, :create], Advertise::Payment do |payment|
          user_confirmed?(user)
        end
        # ===================================

        # ==== Forum ========================
          # Thread
          can [:new, :create], Forum::Thread do
            user_confirmed?(user)
          end
          can :edit, Forum::Thread do |thread|
            thread.user_id == user.id &&
              thread.created_at > 10.minutes.ago
          end
          can [:read, :watching], Forum::Thread do |thread|
            user.profile.institution_website == thread.institution_website
          end
          can :comment, Forum::Thread do |thread|
            user_confirmed?(user) && 
            thread.status == 'open'
          end
          can :destroy, Forum::Thread do |thread|
            thread.user_id == user.id
          end

          # Comment
          can :create, Forum::Comment do |comment|
            user_confirmed?(user) &&
            comment.thread.institution_website == user.profile.institution_website
          end
          can :edit, Forum::Comment do |comment|
            comment.user_id == user.id &&
              comment.created_at > 5.minutes.ago
          end
          can :destroy, Forum::Comment do |comment|
            comment.user_id == user.id 
          end
          
        # ===================================

          # ==== Messages
            can :message, [User, Market::Accommodation,
                           Market::Book] do |messageable|
              user_confirmed?(user) && 
              case messageable.class.name
              when 'User'
                messageable.id != user.id
              when 'Market::Accommodation', 'Market::Book'
                statuses = [Market::Refresh::AVAILABLE, Market::Refresh::OPEN]
                messageable.refresh.user_id != user.id &&
                  statuses.include?(messageable.refresh.status)
              else
                false
              end
            end

            can :read, Message do |message|
              message.watchers.where(user_id: user.id).limit(1).any?
            end

          # === Responses
            can :respond, Message do |message|
              rules = message.watchers.where(user_id: user.id).where(status: 'open').limit(1).any? &&
              message.watchers.where('user_id <> ? AND status=?', user.id, 'open').limit(1).any? # &&
              # can?(:message, message.messageable)

              case message.messageable.class.name
              when 'Market::Accommodation', 'Market::Book'
                refresh = message.messageable.refresh
                allowed_statuses = [Market::Refresh::AVAILABLE, Market::Refresh::OPEN]
                rules = rules && allowed_statuses.include?(refresh.status)
              else
                rules
              end

              rules
            end

        # ====================================
        # ==== Learn

          # = Course
          can [:index, :search, :joined, :show, :questions], Learn::Course

          can [:new, :create, :edit, :update], Learn::Course do
            user_confirmed?(user)
          end

          can :join, Learn::Course do |course|
            user_confirmed?(user) && 
            user.profile.institution_website == course.institution_website
          end

          can :leave, Learn::Course do |course|
            user_confirmed?(user) && 
            user.cached_courses.pluck(:course_id).include?(course.id)
          end

          # = Question
          can [:index, :show, :search], Learn::Question
          can [:create], Learn::Question do |question|
            user_confirmed?(user) &&
              !question.course.nil? &&
              user.profile.institution_website == question.course.institution_website
          end

          can [:edit, :update, :destroy] do |question|
            user_confirmed?(user) &&
              question.user_id == user.id
          end

          # Answer
          can [:create], Learn::Answer do |answer|
            user_confirmed?(user) &&
              !answer.question.nil? &&
              user.cached_courses.pluck(:course_id).include?(answer.question.course.id)
          end

          can [:destroy], Learn::Answer do |answer|
            user.id == answer.id
          end

        # ====================================
      end

      if user.cached_profile_role == 'admin'
        can :manage, :all
      end

    end

  def user_confirmed?(user)
    # TODO: Make sure that the user is confirmed
    # raise user
    if Rails.env == 'production'
      user.confirmed?
    else
      true
    end
  end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
