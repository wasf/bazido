module Market
  class Image < MarketClass # ActiveRecord::Base

    belongs_to :refresh, touch: true

  paperclip_options_display = {default_url: '/displays/small/missing.png', :dependent => :destroy,  :styles => {:original => ['1024x768>', :jpg], :refresh_preview => ['96x96#', :jpg], :small => ['256x256>', :jpg]}, :convert_options => {:refresh_preview => '-strip -quality 65', :small => '-strip -quality 65', :medium => '-strip -quality 85'}}

  unless Rails.env.development?
    paperclip_options_display.merge! :storage        => :s3,
      :s3_credentials => "#{Rails.root}/config/s3.yml",
      :path           => ':attachment/:id/:style.:extension',
      :bucket         => 'bazido-serve'
  end

    has_attached_file :display, paperclip_options_display

  validates_attachment_content_type :display, :content_type => /^image\/(png|gif|jpeg)/
  validates_attachment_size :display, :less_than => 2.megabytes

    before_post_process :check_file_size


    private

    def check_file_size
      valid?
      errors[:display_file_size].blank?
    end

  end
end
