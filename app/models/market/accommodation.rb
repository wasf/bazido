require 'elasticsearch/model'

module Market
  class Accommodation < MarketClass
    include ::IdentityCache

    include PostScopeMixin
    include Filterable

    include ::Elasticsearch::Model
    include ::Elasticsearch::Model::Callbacks

    settings index: { number_of_shards: ::Market::NUMBER_OF_ELASTICSEARCH_SHARDS } do

      mappings dynamic: 'false' do
        indexes :title, analyzer: 'english'
        indexes :description, analyzer: 'english'
        indexes :location, analyzer: 'english'
      end
    end

    # ACCOMMODATION_TYPES = %w()

    has_one :necessity, :dependent => :destroy
    has_one :refresh, :as => :refreshable, :dependent => :destroy
    # has_many :messages, as: :messageable, class_name: 'Message'
    has_many :messages, as: :messageable

    # cache_has_one :necessity, inverse_name: :market_necessity, embed: true
    cache_has_one :refresh, inverse_name: :refreshable
    
    cache_has_many :messages, inverse_name: :messageable

    accepts_nested_attributes_for :refresh, :update_only => true, :allow_destroy => false
    accepts_nested_attributes_for :necessity, :update_only => true, :allow_destroy => false

    before_validation :prepare_data

    validates_presence_of :title
    validates_length_of :title, :within => 10..100
    validates_presence_of :description
    validates_presence_of :deposit
    validates_presence_of :rent
    validates_presence_of :first_payment
    validates_presence_of :location


    def owner_id
      self.refresh.user_id
    end

    # Returns the rent when called by refreshable view in index pages
    def to_amount
      self.rent
    end

    protected

      def fix_string(string)
        # string.gsub(/(\s+)/,' ')
        string.to_s.strip
      end

      def prepare_data
        self.title = fix_string(self.title)
        self.description = fix_string(self.description)
        self.location = fix_string(self.location)
      end
  end

  # begin
    # Rebuild ElasticSearch indexes
    # delete
    Market::Accommodation.__elasticsearch__.client.indices.delete index: Market::Accommodation.index_name rescue nil

    # create
      Market::Accommodation.__elasticsearch__.client.indices.create index: Market::Accommodation.index_name, body: { settings: Market::Accommodation.settings.to_hash, mappings: Market::Accommodation.mappings.to_hash } rescue nil
  # rescue Faraday::ConnectionFailed
  #   # TODO: Report that elastic search is off

  # end
    #
    Market::Accommodation.import rescue nil
    # ==============================
  # rescue Faraday::ConnectionFailed
  #   # TODO: Report that elastic search is off

  # end
end

# # Rebuild ElasticSearch indexes
# # delete
# Market::Accommodation.__elasticsearch__.client.indices.delete index: Market::Accommodation.index_name rescue nil

# # create
# Market::Accommodation.__elasticsearch__.client.indices.create index: Market::Accommodation.index_name, body: { settings: Market::Accommodation.settings.to_hash, mappings: Market::Accommodation.mappings.to_hash }
# #
# Market::Accommodation.import
# # ==============================
