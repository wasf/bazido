module Market
  class Refresh < MarketClass # ActiveRecord::Base
    
    include IdentityCache
    include Filterable

    # Rather just keep the id than reference a model in different namespace
    # belongs_to :user
    EXPIRED = 0
    AVAILABLE = 1
    UNAVAILABLE = 2
    TAKEN = 3
    SOLD = 4
    CANCELLED = 5
    OUT_OF_STOCK = 6
    OPEN = 7
    CLOSED = 8

    ITEM_STATUSES = {:available => AVAILABLE, :sold => SOLD, :out_of_stock => OUT_OF_STOCK, :cancelled => CANCELLED}

    SERVICE_STATUSES = {:available => AVAILABLE, :open => OPEN, :taken => TAKEN, :unavailable => UNAVAILABLE, :closed => CLOSED, :cancelled => CANCELLED}


    # ================================================

    belongs_to :refreshable, :polymorphic => true
    belongs_to :user
    has_many :contacts_refreshes
    has_many :contacts, :through => :contacts_refreshes, :dependent => :destroy
    has_many :images, :dependent => :destroy

    # cache_has_many :images, embed: true

    validates_presence_of :status

    accepts_nested_attributes_for :images, :update_only => true, :allow_destroy => true, :reject_if => :all_blank


    def is_expired?
      if self.refreshable.refresh.status == AVAILABLE && self.refreshable.refresh.expires <= Time.now
        true
      else
        false
      end
    end

    def is_available?
      if self.refreshable.refresh.status == AVAILABLE && self.refreshable.refresh.expires >= Time.now
        true
      else
        false
      end
    end

    def is_open?
      if self.refreshable.refresh.status == OPEN && self.refreshable.refresh.expires >= Time.now
        true
      else
        false
      end
    end

    # Gets the status name because all status names are the same with unique numbers
    def self.status_name(status_array, id)
      name = 'unknown'
      status_array.each do |value, key|
        if id == key
          name = value
          break
        end
      end
      name
    end

    # Get the array of statuses to be used in a select
    def self.statuses(object)

      case object.refreshable_type #.class.name
      when "Market::Accommodation"
        # Return the statuses that must be associated with a accommodation object
        return Refresh.service_statuses

      when "Market::Book"
        # Return the statuses that must be associated with a book object
        return Refresh.item_statuses

      else
        # Return an empty array
        return []

      end
    end

    def self.item_statuses

      statuses = ITEM_STATUSES.collect do |key, value|
        
          [key.to_s.humanize, value]

      end

      return statuses

    end

    def self.service_statuses

      statuses = SERVICE_STATUSES.collect do |key, value|

          [key.to_s.humanize, value]

      end
      
      return statuses

    end

    # Cycles start at 1 and end at 4. They double each time starting from
    # 3.5 days.
    def self.first_cycle(multiplier = 1)
      (multiplier * 3.5).days.from_now
    end

    def self.second_cycle(multiplier = 1)
      (multiplier * 7).days.from_now
    end

    def self.third_cycle(multiplier = 1)
      (multiplier * 14).days.from_now
    end

    def self.fourth_cycle(multiplier = 1)
      (multiplier * 28).days.from_now
    end

    def self.month_cycle(multiplier = 1)
      (multiplier * 30).days.from_now
    end

    def to_mongo
      JSON.parse(to_json(only: [:id, :refreshable_type, :refreshable_id, :next_allowed, :last_allowed, :expires, :status, :is_public, :institution_website, :user_id]))
    end
  end
end
