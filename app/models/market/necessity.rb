module Market
  class Necessity < MarketClass # ActiveRecord::Base
    include IdentityCache

    belongs_to :accommodation, touch: true
  end
end
