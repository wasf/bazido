module Market
  module Mongo
    class Refresh
      include Mongoid::Document

      field :refreshable_type, type: String
      field :refreshable_id, type: Integer

      field :next_allowed, type: DateTime
      field :last_allowed, type: DateTime
      field :expires, type: DateTime

      field :status, type: Integer
      field :is_public, type: Boolean
      field :institution_website, type: String
     
      field :user_id, type: Integer
      field :views, type: Integer


    end
  end
end
