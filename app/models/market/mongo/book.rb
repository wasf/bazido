module Market
  module Mongo
    class Book < MongoClass
      include Mongoid::Document

      # field :title, type: String
      # field :author, type: String
      field :price, type: BigDecimal
      field :isbn, type: String
      field :courses, type: Array
      embeds_one :refresh, class_name: 'Market::Mongo::Refresh'

      # # Returns data mapped by possible course name
      # @filter_by_course_map = %Q{
      #   function (){
      #     var coursesArray = this.courses.split(',');
      #     var cleanCoursesArray = []; //The shorter course names
      #     var id = parseInt(this._id);
      #     var book = this;
      #     coursesArray.forEach(function (value) {
      #       var courseName = value.split(' ').join('').toLowerCase();
      #       cleanCoursesArray.push(courseName);
      #     });

      #       emit(id, {_id: id, isbn: book.isbn, courses: cleanCoursesArray, refresh: book.refresh});
      #   }
      # }

      # @filter_by_course_reduce = %Q{
      #   function (key, values) {
      #     var results = {tag_count: 0};

      #     values.forEach(function(value){
      #       results.tag_count += 1;
      #     });

      #     return results;
      #   }
      # }

      # # scope :filter_by_course, lambda {|courses_array|
      # #   map_reduce(filter_by_course_map, filter_by_course_reduce)
      # #   # all
      # # }

      # def self.filter_by_course#(courses_array)
      #   books = self.map_reduce(@filter_by_course_map, @filter_by_course_reduce).out(replace: 'filter_by_course').collect do |document|
      #     document
      #   end

      #   books[1..books.length]
        
      # end
    end
  end
end
