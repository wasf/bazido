require 'elasticsearch/model'

module Market
  class Book < MarketClass # ActiveRecord::Base
    include ::IdentityCache

    include PostScopeMixin
    include Filterable


    include ::Elasticsearch::Model
    include ::Elasticsearch::Model::Callbacks

    settings index: { number_of_shards: ::Market::NUMBER_OF_ELASTICSEARCH_SHARDS } do

      mappings dynamic: 'false' do
        indexes :title, analyzer: 'english'
        indexes :author#, analyzer: 'english'
        indexes :isbn, analyzer: 'english'
        indexes :comments, analyzer: 'english'
        indexes :courses#, analyzer: 'english'
      end
    end

    has_one :refresh, :as => :refreshable, :dependent => :destroy
    has_many :messages, as: :messageable

    cache_has_one :refresh, inverse_name: :refreshable
    cache_has_many :messages, inverse_name: :messageable

    before_validation :prepare_data

    validates_presence_of :title
    validates_length_of :title, :within => 10..100
    validates_presence_of :author
    validates_presence_of :isbn
    validates_presence_of :condition
    validates_presence_of :price

    accepts_nested_attributes_for :refresh, :update_only => true, :allow_destroy => false

    def owner_id
      self.refresh.user_id
    end

    # Returns the amount called in refreshable view for index pages
    def to_amount
      self.price
    end

    # Returns the json data required by mongo_book model
    # This function leaves out it's relations
    def to_mongo
      json_hash = JSON.parse(to_json(only: [:id, :price, :isbn, :courses]))

      # Change the courses to a Array for the mongo object
      json_hash['courses'] = Book.split_courses_to_mongo_format(json_hash['courses'])

      json_hash['refresh'] = self.refresh.to_mongo
      json_hash
    end

    def self.split_courses_to_mongo_format(courses_string)

      courses = courses_string.to_s.split(',').collect do |value|
        # Clean up each course
        course = value.to_s.strip.split.join('').parameterize
        course
      end

      return courses

    end

    private

      def fix_string(string)
        string = string.to_s.strip
        # string.gsub(/(\s+)/,' ')
      end

      def fix_text(text)
        text = text.to_s.strip
        text.gsub(/(\s{3,})/, '  ')
      end

      def prepare_data
        self.title = fix_string(self.title)
        self.author = fix_string(self.author)
        self.isbn = fix_string(self.isbn.split.join.split('-').join)
        self.condition = fix_string(self.condition)
        self.age = fix_string(self.age)
        self.price = self.price.to_f.abs
        self.edition = self.edition.to_i.abs
        self.comments = fix_string(self.comments)
        self.courses = self.courses.split.join(' ').split(',').sort.join(', ')
      end
  end
  # Rebuild ElasticSearch indexes
  # delete
  Book.__elasticsearch__.client.indices.delete index: Book.index_name rescue nil

  # create
    Book.__elasticsearch__.client.indices.create index: Book.index_name, body: { settings: Book.settings.to_hash, mappings: Book.mappings.to_hash } rescue nil

  Book.import rescue nil
  # ==============================
end

# # Rebuild ElasticSearch indexes
# # delete
# Market::Book.__elasticsearch__.client.indices.delete index: Market::Book.index_name rescue nil

# # create
# Market::Book.__elasticsearch__.client.indices.create index: Market::Book.index_name, body: { settings: Market::Book.settings.to_hash, mappings: Market::Book.mappings.to_hash }
# #
# Market::Book.import
# # ==============================
