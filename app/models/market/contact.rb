module Market
  class Contact < MarketClass # ActiveRecord::Base
    include ::IdentityCache

    # Just keep the ID rather than belong to something of a different namespace
    # belongs_to :user
    # has_and_belongs_to_many :refreshes, :dependent => :destroy
    has_many :contacts_refreshes
    has_many :contacts, :through => :contacts_refreshes

    before_validation :prepare_data

    validates_presence_of :user_id
    validates_presence_of :private_name
    validates_presence_of :name
    validates_length_of :name, within: 1..10
    validates_presence_of :content
    validates_length_of :content, within: 1..120

    scope :belonging_to_user, lambda {|current_user| where(:user_id => current_user.id)}



    def self.for(user)
      # Perform caching here
      contacts = Contact.where(:user_id => user.id).order(:name).limit(50).select(:id, :private_name)
      return_contacts = contacts.collect do |contact|
        [contact.private_name, contact.id]
      end

      return return_contacts
    end

    protected

      def fix_string(string)
        string.split.join(' ')
      end

      def prepare_data
        self.private_name = fix_string(self.private_name)
        self.name = fix_string(self.name)
        self.content = fix_string(self.content)
      end
  end
end
