module Market

  NUMBER_OF_ELASTICSEARCH_SHARDS = 1

  module PostScopeMixin

    extend ActiveSupport::Concern

    included do

        # scope :most_recent, lambda {|iw| includes(:refresh).where('market_refreshes.institution_website=? AND market_refreshes.refreshable_type=? AND market_refreshes.refreshable_id=market_accommodations.id AND market_refreshes.is_public=true', iw, self.name).references(:market_refreshes).order('market_refreshes.last_allowed')}
        scope :most_recent, lambda {|iw| 
          # table_name = self.name.split('::').join('_').downcase
          includes(:refresh).
            where('market_refreshes.institution_website=?', iw).
            where('market_refreshes.refreshable_type=?', self.name).
            where('market_refreshes.refreshable_id='+self.table_name+'.id').
            where('market_refreshes.is_public=true').
            references(:market_refreshes).
            order('market_refreshes.last_allowed')}

    end

  end
end
