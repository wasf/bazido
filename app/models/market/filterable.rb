module Market
  module Filterable

    extend ActiveSupport::Concern

    module ClassMethods
      def apply_filter(filter, user = nil, is_refresh = false)
        possible_filters = %w(freshest newest expired non_available)
        if !filter.nil? && possible_filters.include?(filter)
          # Apply this filter
          send(filter, is_refresh)
        else
          # Apply the default:freshest filter
          freshest(is_refresh)
        end
      end
      
      def freshest(is_refresh = false)
        if !is_refresh
          order('market_refreshes.last_allowed DESC').joins(:refresh).where('market_refreshes.status IN (?, ?)', Refresh::AVAILABLE, Refresh::OPEN).where('market_refreshes.expires > ?', Time.now).references(:market_refreshes)
        else
          order(last_allowed: :DESC).where('status IN (?, ?)', Refresh::AVAILABLE, Refresh::OPEN).where('expires > ?', Time.now)
        end
      end

      def newest(is_refresh = false)
        if !is_refresh
          order(created_at: :DESC).joins(:refresh).where('market_refreshes.status IN (?, ?)', Refresh::AVAILABLE, Refresh::OPEN).references(:market_refreshes)
        else
          order(created_at: :DESC).where('status IN (?,?)', Refresh::AVAILABLE, Refresh::OPEN)
        end
      end

      def expired(is_refresh = false)
        if !is_refresh
          order('market_refreshes.expires DESC').joins(:refresh).where('market_refreshes.status IN (?,?)', Refresh::AVAILABLE, Refresh::OPEN).where('market_refreshes.expires < ?', Time.now).references(:market_refreshes)
        else
          order(expires: :DESC).where('status IN (?,?)', Refresh::AVAILABLE, Refresh::OPEN).where('expires < ?', Time.now)
        end
      end

      def non_available(is_refresh = false)
        if !is_refresh
          order(updated_at: :DESC).joins(:refresh).where("market_refreshes.status NOT IN (?, ?)", Refresh::AVAILABLE, Refresh::OPEN).references(:market_refreshes)
        else
          order(updated_at: :DESC).where('status NOT IN (?, ?)', Refresh::AVAILABLE, Refresh::OPEN)
        end
      end

      def creation_range(is_refresh, start_time, end_time = Time.now)
        if !is_refresh
          joins(:refresh).where('market_refreshes.created_at >= ? AND refresh.created_at <= ?', start_time, end_time).references(:market_refreshes)
        else
          where('created_at >= ? AND created_at <= ?', start_time, end_time)
        end
      end

      def college(is_refresh, institution_website)
        if !is_refresh
          joins(:refresh).where('market_refreshes.institution_website=?', institution_website).references(:market_refreshes)
        else
          where(institution_website: institution_website)
        end
      end

    end

    
  end
  
end
