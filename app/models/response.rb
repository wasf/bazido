class Response < ActiveRecord::Base
  include IdentityCache

  belongs_to :message, touch: true
  belongs_to :user

  validates_presence_of :content

  before_validation :prepare_data

  protected

    def prepare_data
      self.content = self.content.strip
    end
end
