//= require jquery
//= require best_in_place
//= require best_in_place.jquery-ui
//
//= require_tree .

$(document).ready(function() {
  /* Activating Best In Place */
  jQuery(".best_in_place").best_in_place();
});
