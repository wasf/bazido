(function (image_library){

  image_library.readUrl = function(input, id) {
    
    if (input.files && input.files[0]) {

      var reader = new FileReader();

      reader.onload = function (e) {
        jQuery('#'+id).attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);

    }

  };

})(window.ImageLibrary = window.ImageLibrary || {});
