source 'http://rubygems.org'

# gem 'arel', '6.0.0.beta2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0.rc1'
# Use postgresql as the database for Active Record
gem 'pg' # , '0.17.1'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0.0.beta1'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jQuery as the JavaScript library
gem 'jquery-rails', '~> 4.0.0.beta2'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
gem 'jquery-turbolinks', '~> 2.1.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
gem 'unicorn', '4.7.0'
# gem 'puma'

gem 'json', '1.8.2'

# Use Capistrano for deployment
gem 'capistrano-rails', group: :development

gem 'therubyracer', platform: :ruby

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0.0.beta4'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'binding_of_caller'
  gem 'better_errors'

  gem 'rspec-rails', '3.1.0'
  gem 'guard-rspec', '4.3.1'
  gem 'capybara', '2.4.4'
  gem 'factory_girl_rails', '4.5.0'
  gem 'launchy', '2.4.3'
  gem 'database_cleaner', '1.3.0'
end

# These are the gems I want to use
# gem 'rack-mini-profiler'

#===== My engines ============
# TODO: Move gems into engines that use them. I don't know how to do that yet
# gem 'market', :path => 'market'
#=============================
gem 'public_suffix', '1.4.6'
gem 'haml-rails', '0.5.3'
gem 'devise', '3.4.1'
gem 'simple_form', '3.0.2'
gem 'foundation-rails', '~> 5.4.5.0'

gem 'cancancan', '1.9.2'
gem 'paperclip', '4.2.0'
gem 'aws-sdk', '1.61.0'
gem 'fog', '1.25.0'
gem 'delayed_paperclip', '2.8.0'
gem 'sidekiq', '3.3.0'
gem 'cocoon', '1.2.6'
gem 'nestive', '0.6.0'
gem 'best_in_place', '3.0.1'
gem 'kaminari', '0.16.1'

# For memcache and sweepers
gem 'dalli', '2.7.2'
gem 'rails-observers', '0.1.2' # Needed for rails cache_sweeper method

# Using elasticsearch and it's recommended HTTP module
gem 'elasticsearch-model'
gem 'elasticsearch-rails'
# gem 'elasticsearch', '1.0.6'
# gem 'typhoeus', '0.6.9'

# Access to mongodb on a basic level
gem 'mongoid', '4.0.0'

# Paypal
# gem 'activemerchant', '1.45.0'

# Google analytics
gem 'google-analytics-rails', '0.0.6'

# Simplify caching
gem 'identity_cache', '0.2.2'
gem 'cityhash', '0.8.1'
gem 'dalli-elasticache', '0.1.2'

# Allows markdown
# gem 'redcarpet', '3.2.2'

# Will send the assets to amazon s3 servers
group :production do
  gem 'asset_sync'
end

gem 'open4'
gem 'rubber'
